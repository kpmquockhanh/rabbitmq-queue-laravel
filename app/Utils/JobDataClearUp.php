<?php
/**
 * Created by PhpStorm.
 * User: Hungokata
 * Date: 2/18/18
 * Time: 7:31 PM
 */

namespace App\Utils;
use App\Core123\CliEcho;
use App\Utils\MapDataHelper;
use App\Models\Jobs\JobData;
use App\Models\Jobs\JobDataContent;

class JobDataClearUp
{
    private $jobData;
    private $dataContent = array();

    public function __construct(JobData $jobData, $dataContent = array())
    {
        $this->jobData = $jobData;
        $this->dataContent = $dataContent;
    }

    /**
     * Process
     * @param JobData $jobData
     * @param array $dataContent
     */
    public function process()
    {
        $this->clearJobData();
        $this->clearJobDataContent();
    }

    /**
     * remove các từ khoá thừa
     * @param array|string|int $arrKeyWordRemove
     * @param $field
     * @return string
     */
    private function removeStrInvalidInFieldJob($arrKeyWordRemove, $field)
    {
        return trim(str_replace($arrKeyWordRemove, '', mb_strtolower($this->jobData->$field)));
    }

    // Xu ly lam sach job data
    private function clearJobData()
    {
        $keywordRemoveArr     = config('job_global.keyword_clearup.' . $this->jobData->job_country);
        if (!is_array($keywordRemoveArr)) return;

        $keywordJobType       = $this->removeStrInvalidInFieldJob(array_get($keywordRemoveArr, 'listKeywordRemoveTypeJob'), 'job_type');
        $keywordLiteracy      = $this->removeStrInvalidInFieldJob(array_get($keywordRemoveArr, 'listKeywordRemoveLiteracy'), 'job_literacy');
        $keywordSex           = $this->removeStrInvalidInFieldJob(array_get($keywordRemoveArr, 'listKeywordRemoveSex'), 'job_sex');
        $keywordQty           = $this->removeStrInvalidInFieldJob(array_get($keywordRemoveArr, 'listKeywordRemoveQty'), 'job_quantity');
        $keywordExp           = $this->removeStrInvalidInFieldJob(array_get($keywordRemoveArr, 'listKeywordRemoveExp'), 'job_experience');
        $keywordLevel         = $this->removeStrInvalidInFieldJob(array_get($keywordRemoveArr, 'listKeywordRemoveLevel'), 'job_level');
        $keywordSalary        = $this->removeStrInvalidInFieldJob(array_get($keywordRemoveArr, 'listKeywordRemoveSalary'), 'job_salary');

        if (!$keywordQty) $keywordQty           = $this->getDataInFieldJobExtend('so-luong');
        if (!$keywordLiteracy) $keywordLiteracy = $this->getDataInFieldJobExtend('bang-cap');
        if (!$keywordExp) $keywordExp           = $this->getDataInFieldJobExtend('kinh-nghiem');
        if (!$keywordSex) $keywordSex           = $this->getDataInFieldJobExtend('gioi-tinh');


        // Min - Max Salary
        $salaryMinMax = MapDataHelper::convertToSalaryRange($keywordSalary);

        try
        {
            if (isset($salaryMinMax['min_salary']))
                $this->jobData->job_min_salary       = $salaryMinMax['min_salary'];

            if (isset($salaryMinMax['max_salary']))
                $this->jobData->job_max_salary       = $salaryMinMax['max_salary'];

            $this->jobData->job_type       = $keywordJobType;
            $this->jobData->job_literacy   = $keywordLiteracy;
            $this->jobData->job_sex        = $keywordSex;
            $this->jobData->job_quantity   = $keywordQty;
            $this->jobData->job_experience = $keywordExp;
            $this->jobData->job_level      = $keywordLevel;
            $this->jobData->job_salary     = $keywordSalary;
            $this->jobData->save();

        }catch (\Exception $e)
        {
            \Log::error('[JobDataClearUp - clearJobData] ' . $e->getMessage());
        }
    }

    /**
     * Hàm xử lý get thông tin thêm ở trường cần lấy nếu k tồn tại
     * @param $fieldNeed
     * @return null|string
     */
    private function getDataInFieldJobExtend($fieldNeed)
    {
        if (isset($this->dataContent['jdc_extend']))
        {
            $strHtml    = $this->dataContent['jdc_extend'];
            $str        = preg_replace('/(<blockquote>|<\/blockquote>)/','', $strHtml);
            $arrValue   = explode('<br>', $str);
            foreach ($arrValue as $item)
            {
                $item    = strip_tags($item);
                $arrItem = explode(':', $item);
                if(count($arrItem)  == 2)
                {
                    if ($fieldNeed && strpos(trim(str_slug(str_replace("&nbsp;", "", $arrItem[0]))), $fieldNeed) !== false)
                    {
                        return trim(str_replace("&nbsp;", '', $arrItem[1]));
                    }
                }
            }
        }

        return null;
    }


    /**
     * Xu ly lam sach job data content
     */
    private function clearJobDataContent()
    {
        $dataContent = $this->dataContent;
        // Save text content tới bảng tạm
        if (count($dataContent) > 0)
        {
            // Insert database
            try
            {
                $jobContent     = new JobDataContent();
                $jobContentID   = $jobContent->insertGetId($dataContent);
                if ($jobContentID)
                {
                    $jobContent->setTable($dataContent['jdc_job_id'])->insert($dataContent + ['id' => $jobContentID]);
                }
            }catch (\Exception $e)
            {
                \Log::error('[JobDataClearUp - clearJobDataContent] ' . $e->getMessage());
            }
        }
    }
}