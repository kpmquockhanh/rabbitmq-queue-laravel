<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 3/15/18
 * Time: 3:34 PM
 */

namespace App\Core123\Elastic;


use Illuminate\Http\Request;
use ScoutElastic\ElasticEngine;

class ElasticHelper
{
    private $client;
    public function __construct()
    {
        $this->initClientElastic();
    }

    private function initClientElastic()
    {
        $this->client = \Elasticsearch\ClientBuilder::create()->build();
    }

    /**
     * Add a document to elastic api
     * @param array $payLoad
     * @return array
     */
    public function addDocument($payLoad)
    {

        try {
            $this->client->index($payLoad);
            $response = [
                'error' => false,
                'message' => ''
            ];

        } catch (\Exception $e) {
            $response = [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }

        return $response;
    }

    /**
     *  Update document to elastic api
     * @param array $payLoad
     * @return array
     */
    public function updateDocument($payLoad)
    {
        try {
            $this->client->update($payLoad);
            $response = [
                'error'   => false,
                'message' => ''
            ];
        } catch (\Exception $e) {
            $response = [
                'message' => $e->getMessage(),
                'error'   => true
            ];
        }

        return $response;
    }

    public function deleteDocument($payLoad)
    {
        try {
            $this->client->delete($payLoad);
            $response = [
                'message' => '',
                'error'   => false,
            ];
        } catch (\Exception $e) {
            $response = [
                'message' => $e->getMessage(),
                'error'   => true
            ];
        }

        return $response;
    }
}