<?php
namespace App\Utils\Company;
class CompanyHelper
{
    // Replace từ viết tắt
    private $replaceAccentCompany = [
        'cp'                                => 'cổ phần',
        'tm'                                => 'thương mại',
        'tp'                                => 'thực phẩm',
        'vt'                                => 'vận tải',
        'tv'                                => 'tư vấn',
        'xd'                                => 'xây dựng',
        'dv'                                => 'dịch vụ',
        'kt'                                => 'kỹ thuật',
        'sx'                                => 'sản xuất',
        'dl'                                => 'du lịch',
        'tk'                                => 'thiết kế',
        'xđ'                                => 'xây dựng',
        'đt'                                => 'đầu tư',
        'kms'                               => 'khu mua sắm',
        'dvdl'                              => 'dịch vụ du lịch',
        'cn'                                => 'công nghệ',
        'qc' => 'quảng cáo',
        'gn'                                => 'giao nhận',
        'gnvt'                              => 'giao nhận vận tải',
        'dn'                                => 'doanh nghiệp',
        'pt'                                => 'phát triển',
        'dt'                                => 'đầu tư',
        'ql'                                => 'quản lý',
        'qt'                                => 'quốc tế',
        'mtv'                               => 'một thành viên',
        'tnhh.|tnhh|t.n.h.h|t nhh'          => 'trách nhiệm hữu hạn ',
        'ctcp'                              => 'công ty cổ phần',
        'ctđt'                              => 'công trình đô thị',
        'cppt'                              => 'cổ phần phát triển',
        'cnc'                               => 'công nghệ cao',
        'cphh'                              => 'cổ phần hữu hạn',
        'dvsx'                              => 'dịch vụ sản xuất',
        'dvbv'                              => 'dịch vụ bảo vệ',
        'xnk'                               => 'xuất nhập khẩu',
        'dvtm'                              => 'dịch vụ thương mại',
        'ktdv'                              => 'kỹ thuật dịch vụ',
        'tmdv'                              => 'thương mại dịch vụ',
        'cpxd'                              => 'cổ phần xây dựng',
        'cpsx'                              => 'cổ phần sản xuất',
        'cpld'                              => 'cổ phần liên doanh',
        'tđ'                                => 'tập đoàn',
        'cpkt'                              => 'cổ phần kỹ thuật',
        'cpđt'                              => 'cổ phần đầu tư',
        'cpdv'                              => 'cổ phần dịch vụ',
        'cptm'                              => 'cổ phần thương mại',
        'vlxd'                              => 'vật liệu xây dựng',
        'khkt'                              => 'khoa học kỹ thuật',
        'tmsx'                              => 'thương mại sản xuất',
        'bđs|bds'                           => 'bất động sản',
        'qlbđs'                             => 'quản lý bất động sản',
        'tmth'                              => 'thương mại tổng hợp',
        'đtvt'                              => 'điện tử viễn thông',
        'đtpt'                              => 'đầu tư phát triển',
        'đtqt' => 'đầu tư quốc tế',
        'tmdvdl'                            => 'thương mại dịch vụ du lịch',
        'cpqt'                              => 'cổ phần quốc tế',
        'bhnt'                              => 'bảo hiểm nhân thọ',
        'dntn'                              => 'doanh nghiệp tư nhân',
        'ptdv'                              => 'phát triển dịch vụ',
        'côngty|cồng ty|công tu|cổng ty'    => 'công ty',
        'đpt'                               => 'đa phương tiện',
        'gnqt'                              => 'giao nhận',
        'ctld'                              => 'công ty liên doanh',
        'bvtv'                              => 'bảo vệ thực vật',
        'đtsx'          => 'đầu tư sản xuất',
        'xtđt'          => 'xúc tiến đầu tư',
        'tbcn'          => 'thiết bị công nghiệp',
        'xkld|xklđ'                         => 'xuất khẩu lao động',
        'kstk'                              => 'khảo sát thiết kế',
        'tmcp'                              => 'thương mại cổ phần',
        'cnđt'                              => 'công nghệ điện tử',
        'tvtk'                              => 'tư vấn thiết kế',
        'sxtm'                              => 'sản xuất thương mại',
        'sxkd'                              => 'sản xuất kinh doanh',
        'ptht'                              => 'phát triển hạ tầng',
        'cptđ'                              => 'cổ phần tập đoàn',
        'đtxd|dtxd'                         => 'đầu tư xây dựng',
        'đttm'                              => 'đầu tư thương mại',
        'cpcn'                              => 'cổ phần công nghiệp',
        'ptcn'                              => 'phát triển công nghệ',
        'tnhhmtv'                           => 'trách nhiệm hữu hạn một thành viên',
        'cpgd'                              => 'cổ phần giáo dục',
        'tmđt'                              => 'thương mại điện tử',
        'ktts'                              => 'khai thác tài sản',
        'đtdđ'                              => 'điện thoại di động',
        'cptt'                              => 'cổ phần truyền thông',
        'tvđt'                  => 'tư vấn đầu tư',
        'sxđt'                  => 'sản xuất đầu tư',
        'đtkt'  => 'đào tạo kỹ thuật',
    ];

    // chuỗi này thêm cẩn thận k hỏng dữ liệu. Nó có thể thay thế một
    private $arrStringSpecial = [
        'trách nhiệm hữu hạn'       => ' trách nhiệm hữu hạn ',
        'tnhh'                      => ' tnhh ',
        'cntt'                      => ' công nghệ thông tin ',
        'bđs'                       => ' bất động sản ',
        'đầu'                       => ' đầu ',
        'ngan hang'                 => 'ngân hàng',
        'phần'                      => ' phần ',
        'thhh'                      => ' tnhh ',
        'thh'                       => ' tnhh ',
        'xnk'                       => ' xnk ',
        'dvbv'                      => ' dvbv ',
        'đttm'                      => ' đttm ',
        'đtxd'                      => ' đtxd ',
        'đtqt'                      => ' đtxd ',
    ];

    //nếu chuổi ký tự có các key thế này thì mình cho qua.
    private $company_en = [
        'jsc'                  => 'công ty cổ phần',
        'ltd'                  => 'công ty trách nhiệm hữu hạn',
        'công ty'              => 'công ty',
        'ngân hàng'            => 'ngân hàng',
        'chi nhánh'            => 'chi nhánh',
        'company limited'      => 'ltd',
        'group'                => 'group',
        'tập đoàn'             => 'tập đoàn',
        'cổ phần'              => 'cổ phần',
        'trung tâm'            => 'trung tâm',
        'trách nhiệm hữu hạn'  => 'trách nhiệm hữu hạn',
        'doanh nghiệp tư nhân' => 'doanh nghiệp tư nhân'
    ];


    /**
     * Get company by slug
     * @param $string
     * @return string
     */
    public function convertCompanyNameToSlug($string)
    {
        $slug   = '';
        $string = $this->convertCompanyStandard($string);
        if ($string)
        {
            $slug = $this->convertStandardCompanyUniqueText($string);
        }
        return $slug;
    }


    public function removeSpecialFromCompanyText($string)
    {
        $string     = preg_replace('/\(.*\)?/', '', $string);
        $strSpecial = array( chr(9), chr(10), chr(13), '"', "?", ":", "*", "%", "|",
            "/", "\\", "‘", "’", '“', '”', "&nbsp;", '@', '~', '[', ']', '(', ')', "'", "'", '$',',',
            '^',"–", "&quot;","&#34;","\"","&apos;","&#39;","'","&laquo;","&#171;","«","&raquo;","&#187;","»",":",
            "“","”","(",")","!","_","{","}","|","\\","/","%","#","@","^","*","=",";",
            "<",">","...","…");

        $string = str_replace($strSpecial, " ", $string);

        return $string;
    }

    /**
     * Convert công ty đi qua hàm này
     * @param $string
     * @param bool $returnString
     * @param bool $removeSpecial
     * @return mixed|string
     */
    public function convertCompanyStandard($string, $returnString = false, $removeSpecial = true)
    {
        $string               = trim(mb_strtolower($string, 'UTF-8'));
        $replaceAccentCompany = $this->replaceAccentCompany;
        $company_en           = $this->company_en;
        $arrStringSpecial     = $this->arrStringSpecial;

        $arrI = [
            'ð'
        ];

        $arrO= [
            'đ'
        ];

        // pattern chi nhánh
        $patternBranchCompany = '/^(?:chi nhanh|chi nhành|cn.|cn)/';
        $patternCompany       = '/\b(?:c.ty|cong ty|coong ty|cty|ct|công tty|congty)[\b|\s]/ui';
        $string               = strip_tags($string);
        $string               = html_entity_decode($string);

        // tạo khoảng cách cho từ khoá công ty
        $string               = str_replace(['công ty', 'cty'], 'công ty ', $string);
        $string               = str_replace($arrI, $arrO, $string);
        $string               = str_replace(array_keys($arrStringSpecial), array_values($arrStringSpecial), $string);
        $string               = preg_replace($patternBranchCompany, "chi nhánh ", $string);
        $string               = preg_replace($patternCompany, "công ty ", $string);
        $string               = preg_replace('/ +/', ' ', $string);

        $pattern              = array_keys($replaceAccentCompany);
        $replacement          = array_values($replaceAccentCompany);

        foreach ($pattern as $key => $value)
        {
            $pattern[$key] = '/\b'.$value.'\b/ui';
        }

        $string = preg_replace($pattern, $replacement, $string);
        if (str_contains($string, array_keys($company_en)) === false || str_contains($string, ['?']) !== false)
        {
            if ($returnString) return $string;

            return '';
        }

        // Xoá các ký tự đặc biệt
        if ($removeSpecial)
        {
            $string = $this->removeSpecialFromCompanyText($string);
        }

        $replaceAccentCompany = [
            'một thành viên'      => 'mtv ',
            'trách nhiệm hữu hạn' => 'tnhh '
        ];

        $pattern     = array_keys($replaceAccentCompany);
        $replacement = array_values($replaceAccentCompany);
        $string      = str_replace( $pattern, $replacement,$string);

        $lengthText = mb_strlen($string, 'UTF-8');
        if ($lengthText > 80)
        {
            $string = cut_string($string, 80, '');
        }

        $string     = preg_replace('/ +/', ' ', $string);
        $string     = trim($string, '-');
        $string     = trim($string, '.');
        $string     = trim($string);

        return $string;
    }

    /**
     * Chuyển đổi từ khoá dài
     * @param $string
     * @return mixed
     */
    public function convertStandardCompanyUniqueText($string)
    {
        if (!$string) return '';
        $arrSepecial = [
            '&', '/'
        ];

        $string                = str_replace($arrSepecial, '-', $string);
        $string                = str_slug($string);
        $string_remove         = 'cong-ty|vietnam|viet-nam|group|-va-';
        $pattern_string_remove = '/\b'.$string_remove.'\b/ui';
        $string                = preg_replace($pattern_string_remove, '-', $string);
        $string                = preg_replace('/-+/', '-', $string);
        $string                = trim($string, '-');

        return $string;
    }
}