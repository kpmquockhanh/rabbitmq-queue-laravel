<?php
/**
 * Created by PhpStorm.
 * User: Hungokata
 * Date: 2/27/18
 * Time: 10:52 PM
 */

namespace App\Utils;


use App\Core123\CliEcho;
use App\Models\Careers\CareerCategory;
use App\Models\Jobs\JobData;
use Carbon\Carbon;

class MapJobWithCareerCategory
{
    private $jobId;
    /**
     * Xử lý
     * @param int $careerCategoryId : Khoá chính của bảng CareerCategory
     * @param int $jobId : Khoá chính của bảng JobData
     */
    public function process($cc_site_id, $cc_career_md5, $jobId = 0)
    {
        $this->jobId = $jobId;
        $item       = CareerCategory::whereNotNull("cc_career_id")
                            ->where('cc_site_id', $cc_site_id)
                            ->where('cc_career_md5', $cc_career_md5)
                            ->first();
        if ($item)
        {
            // Thay đổi tất cả số bản ghi hiện đang maping với ngành nghề trên site | JobID
            // Đồng thời nghành nghề trên job đó map lại
            if ($jobId)
            {
                $job = JobData::where('id', $jobId)->first();
                if (!$job)
                {
                    CliEcho::warningnl("Không có công việc nào thoả mãn");
                    return false;
                }
                CliEcho::infonl("<pre>".print_r($job->toArray(), true)."</pre>");
                try
                {
                    $this->mapJobWithCareer($job, $item);
                }catch (\Exception $e)
                {
                    \Log::error("JOB " . $job->id . " mapJobWithCareer ". $e->getMessage());
                }
            }
            else
            {
                $start      = Carbon::now()->subDay(40)->toDateTimeString();
                $end        = Carbon::now()->toDateTimeString();
                $jobData    = JobData::where('job_career_site_md5', $item->cc_career_md5)
                                    ->where('site_id', $item->cc_site_id)
                                    ->where('created_at', '>=', $start)
                                    ->where('created_at', '<=', $end);

                $jobData->chunk(100, function ($jobs) use ($item) {
                    foreach ($jobs as $job)
                    {
                        try
                        {
                            $this->mapJobWithCareer($job, $item);
                        }catch (\Exception $e)
                        {
                            \Log::error("JOB " . $job->id . " mapJobWithCareer ". $e->getMessage());
                        }
                    }
                });

            }
        }
    }

    /**
     * Map nghành nghề trong bảng jobData với bảng nghành nghề trên site về category 123job.vn
     * @param $jobData
     */
    private function mapJobWithCareer(JobData $jobData, CareerCategory $careerCategory)
    {
        $arr      = explode(',', $jobData->job_career);
        $arr      = array_filter($arr);
        $arrInsert = [];
        if(count($arr) > 0)
        {
            foreach ($arr as $value)
            {
                $value      = str_replace("&nbsp;", "", $value);
                $md5Slug    = md5(str_slug($value));
                $careerCate =   CareerCategory::where('cc_site_id', $jobData->site_id)
                                    ->whereNotNull('cc_career_id')
                                    ->where('cc_career_md5', $md5Slug)
                                    ->select('cc_career_name', 'id', 'cc_career_id')
                                    ->first();
                if ($careerCate)
                {
                    $arrInsert[$careerCate->cc_career_id] = [
                        "jdc_job_id"    => $jobData->id,
                        "jdc_career_id" => $careerCate->cc_career_id
                    ];
                }
            }
            if (count($arrInsert) > 0)
            {
                try
                {
                    \DB::table('job_datas_careers')->where(	"jdc_job_id", $jobData->id)->delete();
                    \DB::table('job_datas_careers')->insert($arrInsert);
                    \DB::table('careers')->whereIn('id', array_keys($arrInsert))->increment('ca_hit_job');

                }catch (\Exception $e)
                {
                    \Log::error('[ERROR]: '. $e->getMessage());
                }
            }

            \DB::table($jobData->getTable())->where('id', $jobData->id)->update([
                'job_career_cate_id' =>  $careerCategory->cc_career_id,
                'updated_at' => Carbon::now()
            ]);
        }

        if (!$this->jobId)
        {
            $jobData->makeOneSearchableById($jobData->id);
        }
    }
}

