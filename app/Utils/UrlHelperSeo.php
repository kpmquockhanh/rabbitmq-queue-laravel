<?php
/**
 * Created by PhpStorm.
 * User: Hungokata
 * Date: 4/17/18
 * Time: 11:33 PM
 */

namespace App\Utils;


use App\Utils\Keyword\KeywordLibrary;

class UrlHelperSeo
{
    public static $instance = null;
    private $keywordLibraryHelper = null;

    public function __construct()
    {
        $this->keywordLibraryHelper = new KeywordLibrary();
    }

    public static function getInstance()
    {
        if (self::$instance === null)
        {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Convert string to url friendly
     * @param $str
     * @return mixed|string
     */
    private function convertStringUnicodeToUrlFriendly($str)
    {
        $strSpecial = array( chr(9), chr(10), chr(13), '"', "?", ":", "*", "%", "|",
            "/", "\\", "‘", "’", '“', '”', "&nbsp;", '@', '~', '[', ']', '(', ')', "'", "'", '$', '&',
            '^',"–", "&quot;","&#34;","\"","&apos;","&#39;","'","&laquo;","&#171;","«","&raquo;","&#187;","»",":",
            "“","”","(",")","!","_","-","[","]","{","}","|","\\","/","%","#","@","^","*","=",";",
            "<",">","...","…");

        $str = str_replace($strSpecial, " ", $str);
        $str = trim($str);
        $str = trim(preg_replace('/[\s|&|\/]+/', '-', $str));
        $str = preg_replace('/ +/', ' ', $str);
        return mb_strtolower($str);
    }

    public function createUrlSeoKeywordLocation($info)
    {
        $q     = array_get($info, 'q');
        $l     = array_get($info, 'l');
        $type  = array_get($info, 'type', 0);
        $level = array_get($info, 'level', 0);

        $url        = "việc-làm";
        $separator  = '-tại-';

        if ($q)
        {
            $url .= '-'.$this->convertStringUnicodeToUrlFriendly($q);
        }

        if ($l)
        {
            $l    = str_replace(', ', ',', $l);
            $l    = $this->createUrlLocation($type, $level, $l);
            $l    = str_replace(', ', ',', $l);
            $l    = preg_replace('/ +/', '-', $l);
            $l    = preg_replace('/-+/', '-', $l);
            $url .= $separator . $l;
        }

        return trim(trim($url, '-'));
    }

    public function removeKeywordDistrict($str)
    {
        $arrRemove = ['huyện', 'huyen', 'huyên','thành phố', 'thanh phô', 'quận', 'quân', 'thị xã', 'thi xa', 'tỉnh\/thành phố'];

        $sub = '/^((quận|phường).[0-9]{1,2})$/';
        $str       = trim(mb_strtolower($str));

        if(preg_match($sub,$str))
        {
            return $str;
        }

        $arrRemove = implode('|', $arrRemove);
        $str       = preg_replace('/^('.$arrRemove.')(\b|\s)+/iu', '', $str);
        return trim($str);
    }

    public function removePrefixKeywordJob($keyword)
    {
        $pattern        = "/^(việc-làm|việc làm|viec-lam)/";
        $keyword        = mb_strtolower($keyword, 'UTF-8');
        $keyword        = preg_replace($pattern,'', $keyword);
        $keyword        = $this->convertStringUnicodeToUrlFriendly($keyword);
        $keyword        = preg_replace("/[\s-\+]+/", '-', $keyword);
        $keyword        = trim($keyword, '-');
        return $keyword;
    }

    /**
     * Create url location
     * @param string $type
     * @param int $level
     * @param string $location
     * @return mixed|string
     */
    public function createUrlLocation($type = '', $level = 0, $location  = '')
    {
        if ($level > 1)
        {
            if (strpos(mb_strtolower($location) , "quận")  === false &&
                strpos(mb_strtolower($location) , "huyện") === false)
            {
                $location = $type . ' ' . $location;
            }
        }

        $location = str_replace(',', ', ', $location);

        return $location;
    }
}