<?php

namespace App\Core123\File\Images;

use App\Core123\File\Images\ImageFactory;

class ImageFactoryV2 extends ImageFactory {

	public function uploadFromLink($link, $arrayThumbs = [], $optional = 'resize')
	{
		if (empty($arrayThumbs))
		{
			$arrayThumbs = config('image.thumbs');
		}

		$arrayExt = [
		    'gif' => 'gif',
            'jpg' => 'jpg',
            'jpeg' => 'jpeg',
            'png' => 'png'
        ];

		$return = [
			'status'   =>	0,
			'size'     =>	0,
			'filename' =>	'',
			'path'     =>	'',
			'thumbs'   =>	[],
            'status_api' => ''
		];
		$upload 		=	new \App\Core123\File\Uploads\Upload();
		$ext 			=	explode("/", $link);
		$orginal 	=	end($ext);

		$getExt = explode(".", $orginal);
        $extension = end($getExt);
        if(!array_key_exists($extension,$arrayExt)){
            $extension = '.png';
        }else{
            $extension = '';
        }

        $orginal .= $extension;

		$fileName 	=	$upload->generateNewFileName($orginal);


		/*
		if($upload->checkExtension($orginal) == false) {
			throw new App\Core123\File\Uploads\Exceptions\FileTypeIsNotAllowedException($upload->getExtensions());
		}*/

		$content 	=	@file_get_contents($link);

		if(!$content)
			throw new \App\Core123\File\Uploads\Exceptions\UploadPathDoesNotExistException("Đường dẫn không đúng định dạng (http:// hoặc https://).");

		$filesize 	=	strlen($content);

		if($filesize / 1024 > $upload->getFileSizeLimit())
		{
			throw new \App\Core123\File\Uploads\Exceptions\UploadMaxFileSizeException("Upload file tối đa " . $$upload->getFileSizeLimit() . " KB");
		}

		$pathUpload =	$this->upload->getUploadFolderPathToDay() . '/';


        $file =	@file_put_contents($pathUpload . $fileName, $content);
		if(!$file)
		{
			throw new \App\Core123\File\Uploads\Exceptions\UploadPathDoesNotExistException("Không tìm thấy hình ảnh, vui lòng thử lại.");
		}

		$thumbs = [];

		$pathUpload = $this->upload->getUploadFolderPathToDay() . '/';

		if($optional == 'resize')
		{
			 $thumbs = $this->image->resize($pathUpload . $fileName, $pathUpload, $arrayThumbs);
		}
		else if ($optional == 'crop')
		{
			 $thumbs = $this->image->crop($pathUpload . $fileName, $pathUpload, $arrayThumbs);
		}

		list($width, $height) = getimagesize($pathUpload . $fileName);
        $this->checkWidth($fileName);
		
		$return['status']   = 1;
		$return['thumbs']   = $thumbs;
		$return['filename'] = $fileName;
		$return['path']     = $pathUpload . $fileName;
		$return['size']     = filesize($pathUpload . $fileName);
		$return['width']    = $width;
		$return['height']   = $height;
		

		return $return;
	}

}