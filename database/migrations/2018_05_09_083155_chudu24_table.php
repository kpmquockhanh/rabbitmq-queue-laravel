<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Chudu24Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chudu24', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->float('star');
            $table->integer('voteNum');
            $table->string('address');
            $table->text('benefit');
            $table->text('img');
            $table->text('desc');
            $table->text('location');
            $table->text('facilities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
