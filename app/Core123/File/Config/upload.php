<?php

return [
    // Các file được phép upload
    'extensions' => ['gif', 'jpg', 'jpeg', 'png', 'doc', 'docx', 'pdf', 'bmp', 'js', 'css'],

    // 5MB
    'file_size'  => 2560,

    'upload_folder' => 'uploads',

    // Cấu hình url cho ảnh
    'static_url' => '/'
];