<?php

namespace App\Core123\Api\Test;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $table = 'products';

    public function productMeta()
    {
        return $this->hasOne(ProductMetas::class, 'prme_product_id','pro_id');
    }
}
