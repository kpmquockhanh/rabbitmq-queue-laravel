<?php
/**
 * Created by PhpStorm.
 * User: Hungokata
 * Date: 1/26/18
 * Time: 4:18 PM
 */

namespace App\Utils\Keyword;

use App\Core123\Helper\StringHelper;
use Illuminate\Support\Str;

class KeywordLibrary
{
    public static $instance = null;
    private $keywordRemoveSearch = [
        'nhân viên', 'nhan vien'
    ];

    private $keywordRemove = [
        'Việc làm', 'Tuyển dụng',
        'việc làm', 'tuyển dụng',
        "Tuyen dung","tuyen dung",
        "Tuyển sinh", "tuyển sinh",
        "Việc Làm","tuyen sinh",
        "cần tuyển","Viec lam",
        "viec lam", "việc làm tuyển dụng",
        "việc làm tuyen",
        "Tuyển", "tuyển",
        "Cần",
        "cần", "tập đoàn",
        "gấp", "Gấp",
        "Việc", "việc",
        "Tuyen","tuyen", "tuyển",
        "cho", "của",
        "thêm", "Thêm", "hot job", "topcv",
        "mipec", "palace"
    ];

    // từ khoá sẽ remove tất cả sau nhóm mảng này
    private $keywordRemoveAll = [
        "có kinh nghiệm", "không yêu cầu", "yêu cầu", "tại", 'in', 'at', "từ", "khu vực", "miền nam", "miền bắc",
        "miền trung", "kv", "lương", "bằng", "tai", "số", "voi", "ở", "biết", "qua", "theo xe", "cho", "của", "với",
        "địa bàn", "tỉnh", "thành phố", "quận", "huyện", "thu nhập", "up to", "tất cả các", "có sẵn", "lĩnh vực", 'tươi',
        "trong", "không áp", "khối tín", "ca linh hoạt", "được", "kinh nghiệm", "lương cao", "ca linh", "mạng viễn", "không ca",
        "không làm", "làm giờ", "hàng thời trang", "mặc nhà", "trang sức", "sl", "câu lạc bộ", "trung tâm", "thông thạo",
        "viễn", "nhà mạng", "sức khoẻ", "nhà hàng", "truyền hình", "công trường", "nhà máy", "mạng di", "tốt nghiệp", "làm việc",
        "truyền thống", "cn","triệu", "sau", "chưa có", "đã", "chống trộm", "ngân hàng", "và", "vừa", "tối thiểu", "thức ăn", "cung ứng", "địa phương","làm ca", "gian phụ", "phun sương", "mỹ phẩm", "rủi ro", "tòa nhà", "không thu", "nước ngọt", "làm tại",
        "cá nhân","giá thành", "chuyên ca", "viên thông", "viễn thông", "đầu tư", "xe cơ", "đi làm", "chi nhánh","chế độ", "quân đội",
        "không doanh", "vá các", "ưu tiên", "làm tháng", "đến", "trở lên"
    ];

    // từ khoá chuẩn hoá về dạng dấu phẩy trước đó
    private $keywordTheLocationChuanhoa = [
        "tập đoàn", "shop","siêu thị", "công ty", "nhà hàng", "tuyển bảo vệ"
    ];

    // So ki tu min
    public $minLengthTag = 3;

    // So ki tu max
    public $maxLengthTag = 255;

    public static function getInstance()
    {
        $class = __CLASS__;
        if (!static::$instance instanceof $class)
        {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Convert một keyword về dạng chuẩn
     * @param string $keyword
     */
    public function convertKeywordToStandard($keyword="")
    {
        $strToSpace = array('+', '=', '_', '.', ',',"–",'\\');
        $strSpecial = array( chr(9), chr(10), chr(13), "-", '"', ".", "?", ":", "*", "%", "#", "|",
            "/", "\\", ",", "‘", "’", '“', '”', "&nbsp;", '@', '~', '[', ']', '(', ')', "'", "'", '%', '$', '#', '&',
            '^',"–", "&quot;","&#34;","\"","&apos;","&#39;","'","&laquo;","&#171;","«","&raquo;","&#187;","»","?",":",
            "“","”","(",")","!","-","_","[","]","{","}","|","\\","/","%","#","&","@","$","^","&","*","+",".","=",";",
            "<",">","...","…","–");
        $keyword    = str_replace($strToSpace, " ", $keyword);
        $keyword    = str_replace($strSpecial, " ", $keyword);
        $keyword    = preg_replace('/ +/', ' ', $keyword);
        $keyword    = trim($keyword);
        $keyword    = mb_strtolower($keyword, "UTF-8");
        return $keyword;
    }

    /**
     * Remove một keyword chức các tụm từ về việc làm, tuyển dụng
     * @param string $keyword
     */
    private function removeKeywordRelatedTuyendung($string="")
    {
        $string  = preg_replace('/^('. trim(implode('|', $this->keywordRemove)) .')(\b|\s)/i', '', $string);
        return $string;
    }

    public function removeLocationRelatedDistrict($string='')
    {
        $string  = preg_replace('/^('. trim(implode('|',['quận', 'quân', 'quan'])) .')(\b|\s)/i', '', $string);
        return $string;
    }

    /**
     * Lấy từ khoá
     * @param $string
     * @return array
     */
    public function getKeywordStardard($string='')
    {
        if (!$string || mb_strlen($string) < 4) return false;

        $string = $this->removeKeywordRelatedTuyendung($string);

        // Không lấy các từ khoá về việc làm năm cũ
        if ($this->hasYearLess($string))
        {
            return [
                'old_text' => '',
                'new_text' => '',
                'prefix'   => ''
            ];
        }

        $keywords          = (array)$string;
        $keywordsRtn       = [];
        $firstkeyword      = null;
        foreach ($keywords as $key =>  $keyword)
        {
            $keyword = trim($keyword);
            if (!$keyword) continue;

            $keyword = preg_replace('/([\s]+|^)(tại|tai|voi|ở|với)(\b|\s)/', '', $keyword);
            if ($key == 0)
            {
                $firstkeyword = $keyword;
                $keywordsRtn['old_text'] = trim($firstkeyword);
                $keywordsRtn['new_text'] = $this->convertKeywordToStandard($firstkeyword);
            }

            if ($key == 1)
            {
                // key 1 ít hơn từ khoá key 2
                if (mb_strlen($firstkeyword) < mb_strlen($keyword))
                {
                    $prefix = str_replace($firstkeyword, '', $keyword);
                    $keywordsRtn['prefix'] = trim($prefix);
                }
                // key 2 ít hơn từ khoá key 1
                elseif(mb_strlen($firstkeyword) > mb_strlen($keyword))
                {
                    $prefix = str_replace($keyword, '', $firstkeyword);
                    $keywordsRtn['prefix'] = trim($prefix);
                    $keywordsRtn['old_text'] = trim($keyword);
                    $keywordsRtn['new_text'] = $this->convertKeywordToStandard($keyword);
                }
                $firstkeyword = null;
            }
        }
        return $keywordsRtn;
    }

    /**
     * Không lấy các từ khoá về năm cũ
     * @param $keyword
     * @return int
     */
    private function hasYearLess($keyword)
    {
        $years = ['2015', '2013', '2012', '2014', '2016'];
        foreach ($years as $year)
        {
            if ($keyword && strpos($keyword, $year) !== false)
            {
                return 1;
                break;
            }
        }
        return 0;
    }

    /**
     * Xoá các ký tự k chính xác
     * @param $str
     * @return string
     */
    public function clearAllCharacterInvalid($str)
    {
        $str = trim(mb_strtolower($str));

        if (!$str) return '';
        $str = preg_replace('/(\(.*\)|\{.*\}|\[.*\]|[0-9]+[a-z]*[0-9]*|[☆★*$]+|([\s]?+|^)(\b)('.trim(implode("|", $this->keywordRemoveAll)).'|\p{Han}+|[一-龠]+|[ぁ-ゔ]+|[ァ-ヴー]+|[々〆〤]+|tỉnh|thành phố|tp.|quận|mức lương|nhận lương)[\s]?+.*(?>-?))/ui', '', $str);
        $str               = $this->removeCityAndDistrict($str);
        $str               = $this->removeKeywordRelatedTuyendung($str);
        $str               = mb_convert_encoding($str, "UTF-8","UTF-8");
        $str               = str_replace('|', '-', $str);

        $str = preg_replace('/back ?- ?end/', 'backend',$str);
        $str = preg_replace('/front ?- ?end/', 'frontend',$str);

        $splitString       = preg_split('/–|-|_|:/', $str);
        $arrayKeyword      = [];

        foreach ($splitString as $key => $keyword)
        {
            if (!$keyword) continue;
            $splitStringSub = preg_split('/\b(và|va|hoặc|kiêm|mảng)(\b|\s)|(\/|,|\+|&)/', $keyword);
            foreach ($splitStringSub as $subKeyword)
            {
                if ($kw = $this->convertKeywordToStandard($subKeyword))
                {

                    $arrayKeyword[$key][] = $this->buildKeywordChuanHoa($kw);
                }
            }

            if (isset($arrayKeyword[$key]) && count($arrayKeyword[$key]))
            {
                $arrayKeyword[$key] = implode(",", $arrayKeyword[$key]);
            }
        }
        return $arrayKeyword;
    }

    /**
     * Build keyword chuan hoa
     * @param $keyword
     * @return mixed
     */
    private function buildKeywordChuanHoa($keyword)
    {
        foreach ($this->keywordTheLocationChuanhoa as $str)
        {
            if (strpos($keyword, $str) !== false)
            {
                return str_replace($str, ','.$str, $keyword);
            }
        }

        return $keyword;
    }

    /**
     * Build mot regular expression tu file
     * @return string
     */
    private function buildRegularExpresionFromLocation()
    {
        $regexLocation      = @file_get_contents(dirname(__FILE__).'/regex_locations_remove.txt');
        $regexLocationEn    = StringHelper::convertStrToLower($regexLocation, " ");
        $regexLocationVi    = StringHelper::removeAccent($regexLocation);
        return '/\b('.$regexLocation.'|'.$regexLocationEn.'|'.$regexLocationVi.')|([\s]+|^)(tại|tai|voi|ở|với)(.*)/iu';
    }

    /**
     * Hàm này dùng để xoá đi các từ khoá thành phố và quận huyện trong chuỗi string truyền vào
     * @param string $keyword
     * @return mixed|string
     */
    public function removeCityAndDistrict($keyword='')
    {
        if (!$keyword) return '';
        $keyword    = trim($keyword);
        $patternLocation = $this->buildRegularExpresionFromLocation();
        return preg_replace($patternLocation, '', $keyword);
    }

    /**
     * Convert keyword trước khi đưa vào query
     * @param $keyword
     * @return array
     */
    public function convertStandardBeforeQuery($keyword)
    {
        if (get_magic_quotes_gpc() == 1)
        {
            $keyword = str_replace('\"', '"', $keyword);
            $keyword = str_replace("\'", "'", $keyword);
            $keyword = str_replace("'", "", $keyword);
            $keyword = str_replace('\\\\', '\\', $keyword);
        }

        $strToSpace = array('+', '=', '_', '.', ',','\\', "-");
        $strSpecial = array( chr(9), chr(10), chr(13), '"', ".", "?", ":", "*", "%", "#", "|", "/",
            "\\", ",", "‘", "’", '“', '”', "&nbsp;", '@', '~', '[', ']', '(', ')', "'", "'", '%', '$', '#', '&', '^', "&quot;","&#34;",
            "\"","&apos;","&#39;","'","&laquo;","&#171;","«","&raquo;","&#187;","»","?",":","“","”","(",")","!","_","[","]",
            "{","}","|","\\","/","%","#","&","@","$","^","&","*","+",".","=",";","<",">","...","…");

        $keyword    = strip_tags($keyword);
        $keyword    = str_replace($strToSpace, " ", $keyword);
        $keyword    = str_replace($strSpecial, " ", $keyword);
        $keyword    = preg_replace('/ +/', ' ', $keyword);
        $keyword    = trim($keyword, '-');
        $keyword    = trim($keyword);
        $keyword    = mb_strtolower($keyword, "UTF-8");
        return $keyword;
    }

    /**
     * Hàm dùng loại bỏ các ký tự trùng nhau trong một string. Hàm này làm cho elastics search thông tin
     * @param string $tringInput
     * @return string
     */
    public static function makeStringToSearch($stringInput = '')
    {
        if (!$stringInput) return '';

        $tringInput   = mb_strtolower($stringInput, 'UTF-8');
        $tringInput   = Str::substr($tringInput, 0, 80);
        $stringSlug   = str_slug($tringInput, ' ');
        $stringAccent = StringHelper::removeAccent($tringInput);
        $string       = $tringInput. ' ' .$stringSlug. ' ' .$stringAccent;
        $string       = preg_replace('/([\s]+)/', ' ', $string);
        $arrStr       = array_unique(explode(" ", $string));

        return implode(" ", $arrStr);
    }

    /**
     * Replace từ khoá
     * @param $keyword
     * @return mixed
     */
    private function replaceStringWritele($keyword)
    {
        $strWritele = [
            "cv",'nv', 'cskh', 'r&d', '- mysql', "tts", "ctv", 'bđs', 'ktv'
        ];
        $strFull = [
            "chuyên viên",'nhân viên', 'chăm sóc khách hàng', 'r-d', "mysql", "thực tập sinh", "cộng tác viên",
            "bất động sản", "kỹ thuật viên"
        ];

        $keyword   = str_replace($strWritele, $strFull, $keyword);
        return $keyword;
    }

    /**
     * Convert mot string điển hình là title để map với từ khoá cần seo
     * @param string $keyword;
     * @return array;
     */

    public function convertStrToArrayStandard($keyword = [])
    {
        if (!$keyword) return [];

        $keyword    = mb_strtolower($keyword, 'UTF-8');
        $keyword    = $this->replaceStringWritele($keyword);
        $keywords   = $this->clearAllCharacterInvalid($keyword);

        $keywordReturn = [];
        if (count($keywords))
        {
            foreach ($keywords as $item)
            {
                if (trim($item) != '') {
                    $keywordArr = explode(',', $item);
                    if (count($keywordArr) > 0)
                    {
                        foreach ($keywordArr as $value)
                        {
                            if (trim($value) && mb_strlen($value) > 4)
                            {
                                $keywordReturn[] = $value;
                            }
                        }
                    }
                }
            }
        }

        return $keywordReturn;
    }

    /**
     * Method dùng để check từ khoá tiêu đề với từ khoá keyword chính xác thế nào
     * @param string $keywordSearch
     * @param string $keywordResult
     * @return bool
     */
    public function checkArrayDiffKeywordSearch($keywordSearch  = "", $keywordResult = "")
    {
        if(!$keywordSearch || !$keywordResult) return true;

        $keywordSearch        = $this->replaceStringWritele(mb_strtolower($keywordSearch, 'UTF-8'));
        $keywordResult        = $this->replaceStringWritele(mb_strtolower($keywordResult, 'UTF-8'));
        $listWordSearch       = explode(" ", $keywordSearch);
        $listWordResult       = explode(" ", $keywordResult);

        $check = false;
        $wordListDefaults     = [];
        foreach ($listWordResult as $word)
        {
            if ($word != '' && !in_array($word, $listWordSearch))
            {
                $wordListDefaults[] = $word;
                $check = true;
            }
        }

        if ($wordListDefaults)
        {
            $wordListDefault = str_slug(implode(" ", $wordListDefaults), " ");
            $wordListDefault = explode(" ", $wordListDefault);

            $check = false;
            $wordListDefaults = [];
            foreach ($wordListDefault as $wordSlug)
            {
                if ($wordSlug != '' && !in_array($wordSlug, $listWordSearch))
                {
                    $wordListDefaults[] = $wordSlug;
                    $check = true;
                }
            }
        }
        return $check;
    }

    /**
     * Hàm này dùng để search convert một từ khoá search về một mảng từ khoá cho chức năng search
     * @param string $keyword
     * @param int $returnType kiểu trả về
     * @return array|int
     */
    public function processStringBeforeAggs($keyword = '', $returnType= 'str')
    {
        $keyword        = remove_prefix_keyword($keyword);
        $keyword        = $this->convertStandardBeforeQuery($keyword);
        $arrayKeyword   = preg_split("/(?:\s|-|\+)+/", $keyword);
        $arrayKeyword   = array_map("trim", $arrayKeyword);

        if (count($arrayKeyword) !== 2)
        {
            $keyword = trim(str_replace($this->keywordRemoveSearch, '', implode(' ', $arrayKeyword)));
        }
        if ($returnType == 'str')
        {
            return $keyword;
        }
        return $keyword ? preg_split("/(?:\s|-)+/", $keyword) : [];
    }

    /**
     * Tìm từ khoá thành phố, quận huyện trong chuỗi
     * @param $string
     * @return array|void
     */
    public function findAllListCityFromString($string)
    {

        $data = $this->parseLocationFromInput($string);
        $linkRtn = [];
        if ($data)
        {
            foreach ($data as $location)
            {
                $url = create_url_seo_keyword_location(['l'=> $location]);

                $linkRtn[$url] = $this->prefixJob . mb_ucwords($location, 'UTF-8');
            }
        }
        return $linkRtn;
    }

    /**
     * Parse a string location search
     * @param string $string
     * @return array
     */
    public function parseLocationFromInput($string ='')
    {
        $patternLocation = $this->buildRegularExpresionFromLocation();
        $data = [];
        if (preg_match_all($patternLocation, $string, $match))
        {
            if (isset($match[0]) && $match[0])
            {
                $data = array_filter(array_unique($match[0]));
                if ($data)
                {
                    return $data;
                }
            }
        }
        return $data;
    }

    public function findLocationCityFromSearch($string)
    {
        $data =  $this->parseLocationFromInput($string);
        return ($data) ? $data : (array)$string;
    }


    /**
     * Xây dựng một đoạn text mô tả cho trang listing
     *
     * @param $item
     */
    public function generateTextSummary($item, $quering = [])
    {
        $prefixDescription = '';
        $description       = $item->jobDataContent->jdc_description;
        $lengthDescription = mb_strlen($description);
        if ($lengthDescription < 50)
        {
            $prefixDescription .= 'Mức lương: '. $item->job_salary;
            if ($item->job_experience)
            {
                $prefixDescription .= ' - Yêu cầu kinh nghiệm: '. $item->job_experience;
            }

            $description .=  $prefixDescription;
        }

        $contentRequired = $item->jobDataContent->jdc_requiment;
        if ($contentRequired)
        {
            $patternBr = '/(.*?)(?=<br>)/ui';
//            $content = '...';
//            if (preg_match_all($patternBr, $contentRequired, $match))
//            {
//                if (isset($match[0]))
//                {
//                    $lengthRequired = count($match);
//                }
//            }
        }

        $description  = preg_replace('/<[^>]+>/', ' ', $description);
        $description  = cut_string($description, 230);


        if ($search = array_get($quering, 'q'))
        {
            $description = StringHelper::getInstance()->searchKeyword($search, $description);
        }

        $description = preg_replace('/^[-\s]*/', '', $description);

        return $description;
    }
}
