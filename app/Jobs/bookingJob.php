<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Openbuildings\Spiderling\Page;
class bookingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $link;
    public function __construct($link)
    {
        $this->link = $link;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $page = new Page();
        $data = [];
        $page->visit($this->link);

        $data['name'] = $page->find('.hp__hotel-name')->text();

        $data['address'] = $page->find('.hp_address_subtitle')->text();

        $data['vote_num'] = $page->find('.review-score-widget__subtext')->text();

        $data['rate'] = str_replace(',','.', $page->find('.review-score-badge')->text());

        $data['description'] = $page->find('#summary')->html();

        $data['facilities'] = '';
        foreach($page->all('.important_facility ') as $fac)
            $data['facilities'] .= $fac->text().'|';

        \DB::table('booking')->insert([
            'name' => $data['name'],
            'address' => isset($data['address'])?$data['address']:'',
            'vote_num' => isset($data['vote_num'])?$data['vote_num']:'',
            'rate' => isset($data['rate'])?$data['rate']:'',
            'description' => isset($data['description'])?$data['description']:'',
            'facilities' => isset($data['facilities'])?$data['facilities']:'',
            'web' => $this->link,
        ]);
    }
}
