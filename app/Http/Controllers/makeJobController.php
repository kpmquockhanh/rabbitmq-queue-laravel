<?php

namespace App\Http\Controllers;

use App\Jobs\testJob;
use Illuminate\Http\Request;
use \Janiaje\Benchmark\OutputFormats\ArrayFormat;
use Illuminate\Support\Facades\DB;

class makeJobController extends Controller
{
    public static function run()
    {
        benchmark()->checkpoint();
//
        (DB::table('job_datas')->select('*')->orderBy('id')->chunk(3500,function ($jobs){
            foreach ($jobs as $job)
            {
                $matches = [];
                preg_match('/<img src=\"(.*?)\"/',$job->job_desc,$matches);
                $img_link = isset($matches[1])?$matches[1]:'';
                $href_link = $job->page_link;
                dispatch(new testJob($img_link,$href_link));
            }
        }));
        benchmark()->checkpoint();
        echo ("Elapsed time: ".benchmark()->getElapsedTime()->h.':'.benchmark()->getElapsedTime()->i.':'.(benchmark()->getElapsedTime()->s + benchmark()->getElapsedTime()->f));
    }
}