<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Queue;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
//        Queue::before(function (JobProcessing $event) {
//            benchmark()->checkpoint();
//        });
//
//        Queue::after(function (JobProcessed $event) {
//            benchmark()->checkpoint();
//            echo ("Elapsed time: ".benchmark()->getElapsedTime()->h.':'.benchmark()->getElapsedTime()->m.':'.(benchmark()->getElapsedTime()->s + benchmark()->getElapsedTime()->f));
//        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
