<?php
/**
 * Created by PhpStorm.
 * User: Hungokata
 * Date: 1/18/18
 * Time: 2:10 PM
 */

namespace App\Utils\Location;

class LocationHelper
{
    private static $instance = null;
    private $prefixJob = 'Việc làm tại ';
    private $regexLocationDistrict = null;
    private $regexLocationCity = null;


    // keyword k có ý nghĩa trong việc tìm location
    private $keywordRemove = [
        'địa điểm làm việc:',
        'địa điểm làm việc',
        'nơi làm việc',
        'trụ sở chính:',
        'trụ sở chính',
        'việc làm',
        'toàn quốc',
        'địa điểm:',
        'địa điểm',
        'địa chỉ:',
        'địa chỉ',
        'trụ sở:',
        'trụ sở',
        'khác',
        'tại',
        'cn hn',
        'no',
        "street",
        'ngõ',
        'số',
        'city',
        'tầng',
    ];

    private $stringReplace = [
        'bà rịa-vũng tàu'            => 'bà rịa vũng tàu',
        'thừa thiên - huê'           => 'thừa thiên huế',
        'thừa thiên - huế'           => 'thừa thiên huế',
        'thừa thiên- huế'            => 'thừa thiên huế',
        'pleiku'                     => 'plei ku',
        'đakao'             => 'đa kao',
        'kcx'               => ', khu chế xuất',
        "đường"             => ", đường",
        'huyện'             => ', huyện',
        'quận'              => ', quận',
        "trị trấn"          => "thị trấn",
        'kcn'               => 'khu công nghiệp',
        'khu cn'            => 'khu công nghiệp',
        'phường'            => ',phường',
        'district'          => 'quận',
        'khu đô thị'        => ', khu đô thị',
        'ward'              => 'phường',
        'tòa nhà'           => ',tòa nhà',
        'tphcm'             => ',thành phố hồ chí minh',
        'thị xã'            => 'tx',
        'xã'                => ',xã',
        't/p'               => 'thành phố',
        'thành phố'         => ', thành phố',
        'kđt'               => ', khu đô thị',
        'kdc'               => ', khu dân cư',
        'q.'                => ',q.',
        "land"              => "land,",
        'tp hn'             => ',tp hn',
        'tp'                => ',tp',
        'tỉnh'              => ',tỉnh',
        'mỹ đình i'         => 'mỹ đình 1',
        'mỹ đình ii'        => 'mỹ đình 2',
        'Thành phố mới'     => 'thành phố',
        'ha noi'            => 'hà nội',
        'capital'           => '',
        'town'              => '',
        'thủ đô hà nội'     => 'hà nội',
        'lagi'              => 'la gi',
        'thuy phuong'       => 'thuỵ phương',
        'tu liem'           => 'từ liêm'
    ];

    // keywork chứa không cần lấy
    private $keywordContainRemove = [
        'số', 'cụm', 'xóm', 'street', 'km', 'đại lộ', 'tầng', 'khác', 'lô', 'lầu',
        'cơ sở', 'ngõ', 'vp', 'biệt', 'bán', 'khu vực', "toàn quốc",
        'văn phòng', 'tổ', 'ngách', 'vp', 'tower', "thôn", "chi nhánh",
        "vietnam", "email", "sở", "lê", "lot", "ngoài", "chợ", "gặp", "nhân sự",
        "đối diện", "công an", "công ty", "tiểu khu"
    ];

    public static function getInstance()
    {
        if (self::$instance === null)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    // chuẩn hoá chuỗi để bóc tách ok hơn
    private function getStrChuanhoa()
    {
        return $this->stringReplace;
    }
    private function getStrChuanhoaDistrict()
    {
        return [
            'quận 01'           => 'quận 1',
            'quận 02'           => 'quận 2',
            'quận 03'           => 'quận 3',
            'quận 04'           => 'quận 4',
            'quận 05'           => 'quận 5',
            'quận 06'           => 'quận 6',
            'quận 07'           => 'quận 7',
            'quận 08'           => 'quận 8',
            'quận 09'           => 'quận 9',
        ];
    }

    // viết tắt công ty
    private function getCity()
    {
        return [
            'tỉnh qb'                    => 'quảng bình',
            "thành phố hn"               => 'hà nội',
            "thành phố hcm"              => 'hồ chí minh',
            "thành phố hà nội"           => 'hà nội',
            'bà rịa - vũng tàu'          => 'bà rịa vũng tàu',
            "thành phố hồ chí minh"      => 'hà nội',
            "hcmc"                       => "hồ chí minh",
            'tp hn'                      => 'hà nội',
            'tp hà nội'                  => 'hà nội',
            "tp.hcm"                     => 'hồ chí minh',
            'tp hcm'                     => 'hồ chí minh',
            'hn'                         => 'hà nội',
            'hcm'                        => 'hồ chí minh',
            'tphcm'                      => 'hồ chí minh',
            "sapa"                       => "sa pa",
            "đăklăk"                     => "đăk lăk",
            'bắc kạn'                    => 'bắc cạn',
            'bà rịa-vũng tàu'            => 'bà rịa vũng tàu',
            'thừa thiên - huê'           => 'thừa thiên huế',
            'thừa thiên - huế'           => 'thừa thiên huế',
            'thừa thiên- huế'            => 'thừa thiên huế',
            'pleiku'                     => 'plei ku',
        ];
    }

    public static function removeKeywordAdress($address='')
    {
        if(!$address) return '';
        $self = new self();
        $address = trim(mb_strtolower($address));
        $address = preg_replace('/(?:\b|\s?)('.trim(implode('|', $self->keywordRemove)).')(\b|\s)/i', '', $address);
        return trim(trim($address), ',');
    }

    /**
     * Cắt chuỗi địa chỉ
     * @param $address
     * @return array
     */
    public static function splitLocation($address)
    {
        $self    = new self();
        $address = self::removeKeywordAdress($address);

        // Thêm vào các địa chỉ string quận | huyện | xã | tp | phường cách nhau bởi một dấu (, | :)
        $address = str_replace(array_keys($self->getStrChuanhoa()), array_values($self->getStrChuanhoa()), $address);
        return preg_split("/\b(và|cơ sở|co so)(\b|\s)|(;|\/|&)/i", $address);
    }

    /**
     * Get locations
     * @param string $address
     * @return array
     */
    public static function getLocation($address)
    {
        $self          = new self();
        $arrNewAddress = [];
        $arrStrAddress = $self->splitLocation($address);
        //chuẩn hoá lại các quận nếu dữ liệu phường cũng bị thì thêm điều kiện vào mảng | phường 01 => phường 1
        $arrAddress = str_replace(array_keys($self->getStrChuanhoaDistrict()), array_values($self->getStrChuanhoaDistrict()), $arrStrAddress);

        foreach ($arrAddress as $stt => $add)
        {
            $strAdd = trim(mb_strtolower($add));
            $arrAddressSub = preg_split("/-|–|_|,|:/", $strAdd);
            if(count($arrAddressSub) > 0)
            {
                foreach ($arrAddressSub as $str)
                {
                    $str = trim($str);
                    //bóc tách các địa chỉ đường, phố theo số đi cùng
                    if (preg_match('/^[0-9\s]+[a-z]?(\s.*)/', $str, $match))
                    {
                        if (isset($match[1]))
                        {
                            $strPlusDot = str_replace(['p.', 'q.'], [', p.', ', q.'], $match[1]);
                            $arrSplit   = explode(',', $strPlusDot);
                            foreach ($arrSplit as $strSplit)
                            {
                                $arrNewAddress[$stt][] = [
                                    'name' => $self->getLocationShortName($strSplit),
                                    'type' => 4
                                ];
                            }
                        }
                    }

                    // Bóc tách quận huyện trong chuỗi đặc biệt có từ q. hay p. hay q12, q13
                    if (strlen($str) <= 4 && preg_match('/^[\s]?(p[\.\s]?[0-9]{1,2}|q[\.]?[0-9]{1,2})/', $str, $match)) {
                        if (isset($match[0])) {
                            if (strpos($match[0], 'p') !== false) {
                                $str = preg_replace('/p\.|p/', 'phường ', $match[0]);
                            } elseif (strpos($str, 'q') !== false) {
                                $str = preg_replace('/q\.|q/', 'quận ', $match[0]);
                            }

                            if ($str) {
                                $str = preg_replace('/\s+/', ' ', $str);
                                $arrString = explode(' ', $str);
                                if (isset($arrString[1]) && $arrString[1] > 18) {
                                    $str = '';
                                }
                            }
                            if ($str) $arrNewAddress[$stt][] = [
                                'name' => trim($str),
                                'type' => 2
                            ];
                        }
                    }

                    // Xoá các ký tự đặc biệt bắt đầu (
                    if (preg_match('/\(.*/', $str))
                    {
                        $arrNewAddress[$stt][] = [
                            'name' => $self->getLocationShortName($str),
                            'type' => null
                        ];
                    }

                    // Tìm các phường với quận viết tắt trong chuỗi
                    if (preg_match_all('/(p\.[0-9]{1,2})|(q\.[0-9]{1,2})/im', $str, $matches, PREG_SET_ORDER, 0)) {
                        foreach ($matches as $matchOnes) {
                            if ($matchOnes) {
                                foreach ($matchOnes as $oneLo) {
                                    if ($oneLo) {
                                        $arrNewAddress[$stt][] = [
                                            'name' => $self->getLocationShortName($oneLo),
                                            'type' => 3
                                        ];
                                    }
                                }
                            }
                        }
                    }

                    // Từ khoá nếu chứa ký tự vớ vẩn thì cút
                    if ($self->validAddress($str) === false) {
                        $str = $self->getLocationShortName($str);

                        $str = trim(str_replace([
                            "tx.",'.'
                        ], '', $str));

                        $arrNewAddress[$stt][] = [
                            'name'=> $str,
                            'type'=> null,
                        ];
                    }
                }
            }
        }

        // Ghép từ khoá và remove từ khoá
        $arrRtn = [];

        if (count($arrNewAddress)) {
            foreach ($arrNewAddress as $key => $newAddress)
            {
                if (count($newAddress)> 0)
                {
                    foreach ($newAddress as $subKey => $item)
                    {
                        $itemName = trim($item['name']);
                        if ($itemName && mb_strlen($itemName) > 4)
                        {
                            $str = preg_replace('/\s+/', ' ', $itemName);
                            $str = preg_replace('/\(.*\)?$/', '', $str);
                            $arrRtn[$key][] = [
                                'name' => trim($str),
                                'type' => $item['type']
                            ];
                        }
                    }
                    if( isset($arrRtn[$key])) $arrRtn[$key] = array_filter(str_replace($self->getTuKhoaRac(),'',$arrRtn[$key]));
                }
            }
        }

        $arrRtn = $self->removePrefixAddress($arrRtn);

        return $arrRtn;
    }

    /**
     * getLocationShortName
     * @param $str
     * @return mixed
     */
    private function getLocationShortName($str)
    {
        $str = trim(str_replace(["tp.", "tp"], "", $str));
        $str = preg_replace('/\(.*\)?|\)?$/', '', $str);
        $str = trim($str);

        $citys = $this->getCity();

        if (isset($citys[$str])) $str = $citys[$str];

        // replace quận huyện
        $str = str_replace(['q.', 'q '], 'quận ', $str);
        $str = str_replace(['p.'], 'phường ', $str);

        return trim($str);
    }

    /**
     * B
     * @param $haystack
     * @param $needle
     * @return bool|int
     */
    public function validAddress($haystack = '')
    {
        $haystack = trim($haystack);
        if ($haystack == '' || !$haystack) return true;

        // Nhóm ký tự đặc biệt : bắt đầu là số hoặc chữ có . thì cho out
        $pattern = '/^[0-9]+|[a-zA-Z]+[\.]?[0-9]+/';
        if (preg_match($pattern, $haystack)) return true;

        // Nhóm keyword có chứa từ khoá này thì k lấy
        foreach ($this->keywordContainRemove as $what)
        {
            if (($pos = strpos($haystack, $what)) !== false) return $pos;
        }
        return false;
    }

    public function getTuKhoaRac()
    {
        return [
            'địa điểm làm việc',
            'địa chỉ',
            'tòangc',
            'điện thoại',
            'đang cập nhật'
        ];
    }

    /**
     * Remove tiền tố địa chỉ
     * @param array $listAddress
     * @return array
     */
    public static function removePrefixAddress($listAddress=array())
    {
        $self = new self();
        $prefixLocation = [
            [
                'name' => 'tỉnh',
                'type' => 1,
            ],
            [
                'name' => 'tinh',
                'type' => 1,
            ],
            [
                'name' => 'thành phố',
                'type' => 1
            ],
            [
                'name' => 'tp',
                'type' => 1
            ],
            [
                'name' => 'dictrict',
                'type' => 2
            ],
            [
                'name' => 'quận',
                'type' => 2
            ],
            [
                'name' => 'huyện',
                'type' => 2
            ],
            [
                'name' => 'thị trấn',
                'type' => 3
            ],
            [
                'name' => 'tt',
                'type' => 3
            ],
            [
                'name' => 'tx',
                'type' => 3
            ],
            [
                'name' => 'thị xã',
                'type' => 3
            ],
            [
                'name' => 'phường',
                'type' => 3
            ],
            [
                'name' => 'phuong',
                'type' => 3
            ],
            [
                'name' => 'xã',
                'type' => 3
            ],
            [
                'name' => 'đường',
                'type' => 4
            ]
        ];

        $listAddressReturn = [];
        if (!count($listAddress)) return $listAddressReturn;

        foreach ($listAddress as $key => $addresses)
        {
            if (!count($addresses)) continue;
            foreach ($addresses as $val => $address)
            {
                $addressName = $address['name'];
                if ($addressName && mb_strlen($addressName) > 1 )
                {
                    // nếu địa chỉ truyền vào là quân 1->20 or phường 1-20 thì ko replacee
                    $addressName = trim($addressName);
                    //preg_match_all($sub,$addressName) || preg_match_all('/^(.*)(phuong)$/',$addressName,$match)
                    //preg_match_all($sub,$addressName,$match))

                    if((preg_match_all('/^((quận|phường).[0-9]{1,2})$/',$addressName,$match)))
                    {
                        $type = $match[2][0] == 'phường' ? 3 : 2;

                        $listAddress[$key][$val] = [
                            'name' => trim($addressName),
                            'type' => $type
                        ];
                    }else
                    {
                        foreach($prefixLocation as $preloca)
                        {
                            if(strpos($addressName,$preloca['name']) !== false)
                            {
                                $listAddress[$key][$val] = [
                                    'name' => trim(str_replace($preloca['name'],'',$addressName)),
                                    'type' => $preloca['type']
                                ];
                            }
                        }
                    }
                }
            }
            $arrLocations = array();
            // tránh bị trùng
            if (isset($listAddress[$key]))
            {
                $i = 0;
                $key_array = array();
                foreach($listAddress[$key] as $item)
                {
                    if(in_array($item['name'],$self->getTuKhoaRac())) continue;

                    if (!in_array($item['name'], $key_array)) {
                        $key_array[$i] = $item['name'];
                        $arrLocations[] = $item;
                        $listAddressReturn[$key] = $arrLocations;
                    }
                    $i++;
                }
            }
        }

        return $listAddressReturn;
    }

    /**
     * Build regex from file to detect city and district
     * @return bool|mixed|string
     */
    private function getContentFileLocationDistrict()
    {
        if ($this->regexLocationDistrict === null)
        {
            $regexLocation  = @file_get_contents(dirname(dirname(__FILE__)).'/Keyword/location_district.txt');
            $regexLocation  = trim(mb_strtolower($regexLocation, 'UTF-8'));
            $this->regexLocationDistrict = $regexLocation;
        }
        return $this->regexLocationDistrict;
    }

    // Lấy location city tiếng việt
    private function buildRegexFileLocationCity()
    {
        if($this->regexLocationCity === null)
        {
            $regexLocation  = @file_get_contents(dirname(dirname(__FILE__)).'/Keyword/location_city.txt');
            $regexLocation  = trim(mb_strtolower($regexLocation, 'UTF-8'));
            $this->regexLocationCity = $regexLocation;
        }

        return '/\b('.$this->regexLocationCity.')/iu';
    }

    // Parse location from file
    private function parseLocationDistrictUsingRegexFile($str = '')
    {
        if (!$str) return '';
        $district = $this->getContentFileLocationDistrict();
        if (preg_match('/((?:quận|huyện|thị xã|thành phố)?[\s\p{L}]*'.trim($str).'(?:[,\p{L}\s]*+))/ui', $district, $match))
        {
            if (isset($match[0]))
            {
                return $match[0];
            }
        }

        return '';
    }

    public function getListLocationCityDistrictWithLink($str, $link = false, $district= false)
    {
        if (!$str) return [];

        $locationReturn = [];
        $str            = self::removeKeywordAdress($str);
        $str            = mb_strtolower($str, 'UTF-8');
        $str            = str_replace(array_keys($this->getCity()), array_values($this->getCity()), $str);
        $str            = $this->getLocationShortName($str);
        $arrStr         = preg_split('/,|;/', $str);
        $arrStr         = array_map('trim',$arrStr);
        $arrStr         = array_unique($arrStr);

        if ($arrStr)
        {
            $patternCity = $this->buildRegexFileLocationCity();
            foreach ($arrStr as $string)
            {
                $item = null;
                if (preg_match($patternCity, $string))
                {
                    $item = $string;
                }

                if ($item  === null && $district == true)
                {
                    $str = str_replace(array_keys($this->getStrChuanhoaDistrict()), array_values($this->getStrChuanhoaDistrict()), $string);

                    $items = $this->parseLocationDistrictUsingRegexFile($str);
                    $item  = $items;
                }

                if ($item = trim($item))
                {
                    $value       = str_replace(',', ', ', $item);
                    $valueUcword = mb_ucwords($value, 'UTF-8');

                    if ($link === true)
                    {
                        $url = create_url_seo_keyword_location(['l'=> $valueUcword]);
                        $locationReturn[$url] = $this->prefixJob . $valueUcword;
                    }
                    else
                    {
                        $locationReturn[$item] = $valueUcword;
                    }
                }
            }
        }

        return $locationReturn;
    }
}