<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Kenhnhansu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kenhnhansu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chucdanh');
            $table->string('congty');
            $table->string('luong');
            $table->string('diadiem');
            $table->date('thoigian');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
