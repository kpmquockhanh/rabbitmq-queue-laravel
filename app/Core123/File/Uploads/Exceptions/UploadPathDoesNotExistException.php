<?php namespace App\Core123\File\Uploads\Exceptions;

class UploadPathDoesNotExistException extends \Exception {}