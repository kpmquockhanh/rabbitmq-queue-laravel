<?php

namespace App\Core123\File\Images;

use App\Core123\File\Uploads\Uploader;

class ImageFactory {

    protected $upload;

    protected $image;

    public $key;

    public function __construct(Uploader $upload, Image $image) {
        $this->upload = $upload;
        $this->image  = $image;
        $this->maxWidth =  config('image.max_width');
    }

    public function upload($fileControl, $arrayThumbs = [], $optional = 'resize') {
        $this->upload->key = $this->key;
        if (empty($arrayThumbs))
        {
            $arrayThumbs = config('image.thumbs');
        }

        $return = [
            'status'   => 0,
            'size'     => 0,
            'filename' => '',
            'path'     => '',
            'thumbs'   => []
        ];

        $dataFileName = $this->upload->upload($fileControl);

        $fileName = $dataFileName['files'];
        $return['status_api'] = $dataFileName['status'];
        if($fileName)
        {
            $thumbs = [];

            $pathUpload = $this->upload->getUploadFolderPathToDay() . '/';

            if($optional == 'resize')
            {
                $thumbs = $this->image->resize($pathUpload . $fileName, $pathUpload, $arrayThumbs);
            }
            else if ($optional == 'crop')
            {
                $thumbs = $this->image->crop($pathUpload . $fileName, $pathUpload, $arrayThumbs);
            }

            list($width, $height) = getimagesize($pathUpload . $fileName);
            $this->checkWidth($fileName);

            $return['status']   = 1;
            $return['thumbs']   = $thumbs;
            $return['filename'] = $fileName;
            $return['path']     = $pathUpload . $fileName;
            $return['size']     = filesize($pathUpload . $fileName);
            $return['width']    = $width;
            $return['height']   = $height;
        }

        return $return;
    }


    public function uploadMulti($fileControl, array $arrayThumbs = array(), $action = 'resize')
    {
        if (empty($arrayThumbs))
        {
            $arrayThumbs = config('image.thumbs');
        }

        $arrayResult = [
            'filename' => array(),
            'thumbs'   => array()
        ];

        $pathUpload = $this->upload->getUploadFolderPathToDay() . '/';

        $resulUpload = $this->upload->uploadMulti($fileControl, $pathUpload);


        foreach($resulUpload as $k => $fileName) {
            $thumbs = array();
            if($action == 'resize') {
                $thumbs = $this->image->resize($pathUpload . $fileName, $pathUpload, $arrayThumbs);
            } else if ($action == 'crop') {
                $thumbs = $this->image->crop($pathUpload . $fileName, $pathUpload, $arrayThumbs);
            }

            $this->checkWidth($fileName);

            $arrayResult['filename'][] = $fileName;
            $arrayResult['thumbs'][] = $thumbs;
        }

        return $arrayResult;
    }

    public function checkWidth($fileName)
    {
        $pathUpload     =    $this->upload->getUploadFolderPathToDay() . '/';
        list($width, $height) = getimagesize($pathUpload . $fileName);
        if($width > $this->maxWidth)
        {
            $height =   (int) $this->maxWidth * $height / $width;
            $width  =   (int) $this->maxWidth;    

            $this->image->resize($pathUpload . $fileName, $pathUpload, ['' => ['width' => $width, 'height' => $height]]);
        }
    }
}