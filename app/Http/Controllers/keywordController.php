<?php

namespace App\Http\Controllers;

//use LanguageDetection\Language;
//use Illuminate\Http\Request;
use App\Jobs\keywordJob;
use Illuminate\Support\Facades\DB;

include_once __DIR__.'/../../simple_html_dom.php';

class keywordController extends Controller
{
    //
    public $values = [];
    public function run()
    {

        DB::table('title_keyword_backup')->orderBy('id')->chunk(1000, function ($items) {
            foreach ($items as $item)
            {
                $matches = [];
                $original = $item->title;
//                dd($original);
                if (preg_match('/\[.*?\]/',$item->title,$matches))
                    foreach ($matches as $match)
                        $item->title = str_replace($match,'',$item->title);
                if (preg_match('/\(.*?\)/',$item->title,$matches))
                    foreach ($matches as $match)
                        $item->title = str_replace($match,'',$item->title);
                $item->title = mb_strtolower($item->title);
                $keyToRemoves = [
                    'chất lượng',
                    'topcv',
                    'tuyển dụng',
                    'tuyển ',
                    'mipec ',
                    'palace ',
                    'phụ trách ',
                    'chương trình ',
//                    'thực tập sinh ',
                    'giám đốc ',
                    'trợ lí ',
                    'trợ lý ',
                    'cán bộ ',
                    'itenrn ',
//                    'và ',
                    ',',
//                    'tư vấn',
//                    'chăm sóc',
//                    'khách hàng',
//                    'quản trị',
                    'lễ tân',
//                    'cộng tác viên',
//                    'giám sát',
                    'giáo viên',
                    'dạy',
//                    'bán hàng',
                    'cao cấp',
//                    'giảng viên',
                    'dự án',
                    'trưởng nhóm',
                    'mua sắm',
                    '&',
                    'làm việc',
                    'tại',
                    'tổng giám đốc',
                    'phó',
//                    'quản lý',
                    'chuyên gia',
                    'kỹ sư',
                    'trưng bày',
                    'phân tích',
                    'quan hệ',
                    'thợ cả',
                    'thi công',
                ];
                foreach ($keyToRemoves as $key)
                    $item->title = str_replace($key,'',$item->title);
//                if(str_replace(' ','',$item->title) !='' )
//                {
//                    echo $original.'</br>';
                    $title_key = [
                        'title' =>$original,
                        'key' => $item->title,
                    ];
                    array_push($this->values,$title_key);
//                }
//                dispatch(new keywordJob($item->id,$item->title));

            }
        });
        $viewData = [
            'datas'=> $this->unique_multidim_array($this->values, 'key')
        ];
        foreach ($this->values as $val)
            dispatch(new keywordJob($val['title'],$val['key']));
        return view('keyword')->with($viewData);
    }

    private function unique_multidim_array($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

//    private function isNounEng($text)
//    {
//        $html = file_get_html('https://en.oxforddictionaries.com/definition/'.urlencode($text));
//        if (!strpos($html->plaintext,'No exact matches found for'))
//            echo ("<strong>$text</strong>" .' là '.$html->find('span.pos')[0]->plaintext).'</br>';
//        else
//            echo "<strong>$text</strong>".' khong co -> delete</br>';
//    }
//    private function isNounVi($text)
//    {
//        $html = file_get_html('http://tratu.coviet.vn/hoc-tieng-anh/tu-dien/lac-viet/V-V/'.urlencode($text).'.html');
//        if (!strpos($html->plaintext,'Dữ liệu đang được cập nhật'))
//            echo ("<strong>$text</strong>" .' là '.$html->find('div.ub')[0]->plaintext).'</br>';
//        else
//            echo "<strong>$text</strong>".' khong co -> delete</br>';
//
//    }
}
