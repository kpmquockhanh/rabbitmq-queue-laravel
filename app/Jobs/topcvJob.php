<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class topcvJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $link;
    public function __construct($link)
    {
        //
        $this->link = $link;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        echo getcwd();

        $data = [];
        $html = new \simple_html_dom($this->curl($this->link,false));

        $data['name'] = $html->find('.company-name',0)->plaintext;
        if (!empty($html->find('.detail-item')))
            foreach($html->find('.detail-item') as $item)
                if (preg_match('/\d{8,15}/', str_replace(' ','',$item->plaintext)))
                    $data['phone'] = $item->plaintext;
                else
                    if (empty($item->find('a')))
                        $data['nv'] = $item->plaintext;
                    else
                        $data['website'] = $item->plaintext;
        else
        {
            $html1 = new \simple_html_dom($this->curl('https://www.topcv.vn/login',false));

            $info = [
                'email' => 'kpmquockhanh@gmail.com',
                'password' => 'khanh1234',
                '_token' => $html1->find('input[name=_token]',0)->value,
            ];

            new \simple_html_dom($this->curlPost('https://www.topcv.vn/login',false, $info));

            foreach($html->find('.detail-item') as $item)
                if (preg_match('/\d{8,15}/', str_replace('.','', str_replace(')','', str_replace('(','', str_replace('-','', str_replace(' ','',$item->plaintext)))))))
                    $data['phone'] = $item->plaintext;
                else
                    if (empty($item->find('a')))
                        $data['nv'] = $item->plaintext;
                    else
                        $data['website'] = $item->plaintext;
        }
        $data['ava'] = $html->find('.company-avatar img',0)->src;
        if (!empty($html->find('#company-about p')))
            $data['desc'] = $html->find('#company-about',0)->innertext;
        $data['addr'] = $html->find('.company-map',0)->prev_sibling()->plaintext;


        DB::table('topcv')->insert([
            'name' => html_entity_decode($data['name']),
            'phone' => isset($data['phone'])?$data['phone']:'',
            'nv' => isset($data['nv'])?$data['nv']:'',
            'website' => isset($data['website'])?$data['website']:'',
            'ava' => isset($data['ava'])?$data['ava']:'',
            'description' => isset($data['desc'])?$data['desc']:'',
            'addr' => isset($data['addr'])?$data['addr']:'',
            'link' => $this->link,
        ]);
    }
    function curl($url,$header)
    {
        $data = curl_init();
        curl_setopt($data, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($data, CURLOPT_URL, $url);
        curl_setopt($data, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($data, CURLOPT_HEADER  , $header);
        curl_setopt($data, CURLOPT_ENCODING,       'gzip,deflate'  );
        curl_setopt($data, CURLOPT_COOKIEJAR, 'public/tmp/cookies.txt');
        curl_setopt($data, CURLOPT_COOKIEFILE, 'public/tmp/cookies.txt');
        curl_setopt($data, CURLOPT_SSL_VERIFYPEER, FALSE );
        curl_setopt($data,CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36');
        $result = curl_exec($data);
        curl_close($data);
        return $result;
    }
    function curlPost($url,$header, $info)
    {

        $data = curl_init();
        curl_setopt($data, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($data, CURLOPT_URL, $url);
        curl_setopt($data, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($data, CURLOPT_COOKIEJAR, 'public/tmp/cookies.txt');
        curl_setopt($data, CURLOPT_COOKIEFILE, 'public/tmp/cookies.txt');
        curl_setopt($data, CURLOPT_HEADER  , $header);
        curl_setopt($data, CURLOPT_ENCODING,       'gzip,deflate'  );
        curl_setopt($data, CURLOPT_SSL_VERIFYPEER, FALSE );
        curl_setopt($data,CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36');
        curl_setopt($data, CURLOPT_POSTFIELDS, http_build_query($info));
        $result = curl_exec($data);
        curl_close($data);
        return $result;
    }
}
