<?php

namespace App\Jobs;

include_once __DIR__.'/../simple_html_dom.php';

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class viecITjob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $link;
    public function __construct($link)
    {
        $this->link = $link;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $html = new \simple_html_dom($this->curl($this->link,false));


            $banner = $html->find('.company-page .cover-images-cropped img',0)?$html->find('.company-page .cover-images-cropped img',0)->src:'';

            $logo = $html->find('.headers .logo-container .logo img',0)?$html->find('.headers .logo-container .logo img',0)->src:'';

            $name = $html->find('.headers .name-and-info h1',0)?$html->find('.headers .name-and-info h1',0)->plaintext:'';

            $location = $html->find('.headers .name-and-info span',0)?$html->find('.headers .name-and-info span',0)->plaintext:'';

            $type = ($html->find('.headers .name-and-info .company-info .gear-icon',0)?$html->find('.headers .name-and-info .company-info .gear-icon',0)->plaintext:'');

            $scale = ($html->find('.headers .name-and-info .company-info .group-icon',0)?$html->find('.headers .name-and-info .company-info .group-icon',0)->plaintext:'');

            $country = ($html->find('.headers .name-and-info .company-info .country',0)?$html->find('.headers .name-and-info .company-info .country',0)->plaintext:'');

            $workingDate = ($html->find('.headers .name-and-info .working-date',0)?$html->find('.headers .name-and-info .working-date',0)->plaintext:'');

            $overTime = ($html->find('.headers .name-and-info .overtime',0)?$html->find('.headers .name-and-info .overtime',0)->plaintext:'');

            $des = ($html->find('.company-container .col-left .panel-default',0)?$html->find('.company-container .col-left .panel-default',0)->outertext:'');

            \DB::table('viecIT')->insert([
                'name' => $name,
                'img' => $logo,
                'banner' => $banner,
                'location' => $location,
                'type' => $type,
                'scale' => $scale,
                'country' => $country,
                'workingdate' => $workingDate,
                'OT' => $overTime,
                'desc' => $des,
                'link' => $this->link,
            ]);
        }
        catch(\Exception $e)
        {
            echo $e->getMessage();

        }
    }
    function curl($url,$header)
    {
        $data = curl_init();
        curl_setopt($data, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($data, CURLOPT_URL, $url);
        curl_setopt($data, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($data, CURLOPT_HEADER  , $header);
        curl_setopt($data, CURLOPT_ENCODING,       'gzip,deflate'  );
        curl_setopt($data, CURLOPT_COOKIEJAR, 'tmp/cookies.txt');
        curl_setopt($data, CURLOPT_COOKIEFILE, 'tmp/cookies.txt');
        curl_setopt($data, CURLOPT_SSL_VERIFYPEER, FALSE );
        curl_setopt($data,CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36');
        $result = curl_exec($data);
        curl_close($data);
        return $result;
    }
}
