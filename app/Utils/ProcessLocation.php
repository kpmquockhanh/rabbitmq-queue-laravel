<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 1/26/18
 * Time: 4:46 PM
 */

namespace App\Utils;

use App\Utils\Location\LocationHelper;
use DB;
use Illuminate\Support\Str;

class ProcessLocation
{
    /**
     * @param string $address
     * @return array
     */
    public static function getLocationId($addressDefault)
    {
        $that = new self();
        $listAddress = LocationHelper::getLocation($addressDefault);
        $listAddress = LocationHelper::removePrefixAddress($listAddress);
        $locationArr = [];

        // mang gia tri location
        foreach($listAddress as $key => $items)
        {
            $totalItem      = count($items);
            foreach($items as $val => $defaultLoc )
            {
                //1. Bắt đầu query để lấy thông tin location tương ứng với $defaultLoc.
                $slugLocation = str_slug($defaultLoc['name']);

                // kiem tra neu location ton tai type thi query theo type khong thi query bt
                if ($defaultLoc['type'])
                {
                    // chua chac chan
                    $allLocations = $that->getLocationBySlug($slugLocation,$defaultLoc['type']);
                }
                else
                {
                    // kiem tra  vs level = 1
                    $allLocations = $that->getLocationBySlug($slugLocation,1);
                    if( $allLocations->count()  == 0 )
                    {
                        // Query - lv2
                        $allLocations           = $that->getLocationBySlug($slugLocation, 2);
                        $lengthAllLocationLv2   = $allLocations->count();
                        if($lengthAllLocationLv2  > 0)
                        {
                            if ($totalItem > 3)
                            {
                                $checkItemExistInLevel2    = false;
                                $moreDescriptionLevelTwo   = $that->convertItemArrSlug($allLocations->pluck('loc_description'));
                                $currentAddressArr         = $that->convertValueStringOrArrayToSlug($that->removeItemFromArr($defaultLoc, $items));
                                foreach ($currentAddressArr as $moreDetect)
                                {
                                    foreach ($moreDescriptionLevelTwo as $moreDescSlug)
                                    {
                                        if( ! $moreDetect)
                                        {
                                            $checkItemExistInLevel2 = true;
                                        }else
                                        {
                                            if (strpos($moreDescSlug, $moreDetect) !== false)
                                            {
                                                $checkItemExistInLevel2 = true;
                                            }
                                        }

                                    }
                                }
                            }
                        }

                        // check tiep vs cac dieu kien level 3
                        // Query - lv3
                        if ($lengthAllLocationLv2 == 0 || (isset($checkItemExistInLevel2) && $checkItemExistInLevel2 !== true))
                        {
                            $allLocations           = $that->getLocationBySlug($slugLocation, 3);
                            $lengthAllLocationLv3   = $allLocations->count();
                            if ($lengthAllLocationLv3 > 0)
                            {
                                $checkItemExistInLevel3    = false;
                                $moreDescriptionLevelThree = $that->convertItemArrSlug($allLocations->pluck('loc_description'), 2);
                                $currentAddressArr         = $that->convertValueStringOrArrayToSlug($that->removeItemFromArr($defaultLoc, $items));
                                // Đoạn này để check level 3 phải có chứa thêm các điều kiện đi kèm. Vì quá nhiễu record.
                                foreach ($currentAddressArr as $moreDetect)
                                {
                                    foreach ($moreDescriptionLevelThree as $moreDescSlug)
                                    {
                                        if( !$moreDetect )
                                        {
                                            $checkItemExistInLevel3 = true;
                                        }else
                                        {
                                            if (strpos($moreDescSlug, $moreDetect) !== false)
                                            {
                                                $checkItemExistInLevel3 = true;
                                            }
                                        }

                                    }
                                }

                                // Đã qua điều kiện check đi kèm. Nhưng còn tồn tại 1 bản ghi mà ở đó chứa dữ liệu thật.
                                // Điểm ( cụm) công nghiệp Di Trạch xã Di trạch
                                if ($checkItemExistInLevel3 === false)
                                {
                                    if ($totalItem < 3 && $lengthAllLocationLv3 == 1)
                                    {
                                        $checkItemExistInLevel3 = true;
                                    }
                                }
                            }

                            // Query - lv4
                            if ($lengthAllLocationLv3 == 0 || (isset($checkItemExistInLevel3) && $checkItemExistInLevel3 !== true))
                            {
                                $allLocations = $that->getLocationBySlug($slugLocation, 4);
                            }
                        }
                    }
                }

                $allLocationsTemp = [];
                $lengthLocation   = $allLocations->count();
                if($lengthLocation > 0)
                {
                    $allLocations   = $allLocations->toArray();
                    foreach($allLocations as $flag =>  $locationItem)
                    {
                        if($locationItem)
                        {
                            if($locationItem->loc_level == 1)
                            {
                                $locationArr[$key][$locationItem->id] = [
                                    'id'     => $locationItem->id,
                                    'level'  => 1,
                                ];
                            }
                            elseif($locationItem->loc_level == 2)
                            {
                                $currentAddressArr         = $that->removeItemFromArr($defaultLoc, $items);
                                $currentAddressArr         = $that->convertValueStringOrArrayToSlug($currentAddressArr);
                                $descriptionMoreDbArray    = $that->convertValueStringOrArrayToSlug($locationItem->loc_description);
                                $checkIntersect             = false;
                                if (count($allLocations) > 1)
                                {
                                    if (!empty(array_intersect($currentAddressArr, $descriptionMoreDbArray)))
                                    {
                                        $checkIntersect = true;
                                    }
                                }
                                if ($checkIntersect == true || $lengthLocation == 1)
                                {
                                    $locationArr[$key][$locationItem->id] = [
                                        'id'     => $locationItem->id,
                                        'level'  => 2,
                                    ];
                                    $locationArr[$key][$locationItem->loc_parent_id] = [
                                        'id'     => $locationItem->loc_parent_id,
                                        'level'  => 1,
                                    ];
                                }
                            }
                            else // Trường hợp xảy ra chỉ dành cho xã, phường, khách sạn, nhà hàng, khu công nghiệp, đô thị, siêu thị
                            {
                                // Xóa từ khóa search khỏi cái mảng item
                                // kiem tra xem mang $items co bao nhieu phan tu ! neu $item chi co 1 phan thu thi khong remove nua
                                $currentAddressArr         = $that->removeItemFromArr($defaultLoc, $items);
                                $descriptionMoreDbArray    = $that->convertValueStringOrArrayToSlug($locationItem->loc_description);
                                $lengthCurrentAddress      = count($currentAddressArr);

                                // Danh cho 1 tu
                                if ($lengthCurrentAddress && $lengthCurrentAddress < 2 || $lengthLocation == 1)
                                {
                                    // Logic code
                                    $currentAddressSlug     = Str::slug(implode("", $currentAddressArr));
                                    $descriptionMoreDbSlug  = implode(",", $descriptionMoreDbArray);
                                    // check chứa từ khóa đi kèm với nó trong phần mô tả không. Nếu có là ok

                                    // currentAddressSlug != null
                                    if($currentAddressSlug && strpos($descriptionMoreDbSlug, $currentAddressSlug) !== false || $lengthLocation == 1)
                                    {
                                        $locationArr[$key][$locationItem->id] = [
                                            'id'     => $locationItem->id,
                                            'level'  => $locationItem->loc_level,
                                        ];
                                        $locationArr[$key][$locationItem->loc_city_id] = [
                                            'id'     => $locationItem->loc_city_id,
                                            'level'  => 1,
                                        ];
                                        $locationArr[$key][$locationItem->loc_district_id] = [
                                            'id'     => $locationItem->loc_district_id,
                                            'level'  => 1,
                                        ];
                                    }
                                }
                                else
                                {
                                    // Remove mô tả chỉ lấy quận huyện|tỉnh cho việc so khớp tránh bị nhiễu data
                                    if (count($descriptionMoreDbArray) > 2)
                                    {
                                        $descriptionMoreDbArray = array_splice($descriptionMoreDbArray, -2, 2);
                                    }
                                    $descriptionMoreDbSlug = implode(",", $descriptionMoreDbArray);
                                    foreach ($currentAddressArr as $strCurrentAddress)
                                    {
                                        // check truong hop 2 kq tra ve cung  tinh/tp nhung khac quan/huyen
                                        $currentAddressSlug = Str::slug($strCurrentAddress);
                                        if ( $currentAddressSlug && strpos($descriptionMoreDbSlug,$currentAddressSlug) !== false )
                                        {
                                            // Thang nay co du lieu va chua keyword dang search
                                            if (isset($allLocationsTemp[$key]) && array_key_exists($locationItem->id, $allLocationsTemp[$key]))
                                            {
                                                $allLocationsTemp[$key][$locationItem->id]['hit'] += 1;
                                            }else
                                            {
                                                $allLocationsTemp[$key][$locationItem->id] = [
                                                    'id'        => $locationItem->id,
                                                    'city'      => $locationItem->loc_city_id,
                                                    'district'  => $locationItem->loc_district_id,
                                                    'name'      => $locationItem->loc_name,
                                                    'desc'      => $locationItem->loc_description,
                                                    'type'      => $locationItem->loc_type,
                                                    'hit'       => 1
                                                ];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (isset($allLocationsTemp[$key]))
                    {
                        //  lay du lieu co hit = max
                        $lengthAllLocationTemp = count($allLocationsTemp[$key]);
                        if ($lengthAllLocationTemp > 0)
                        {
                            if ($lengthAllLocationTemp == 1)
                            {
                                $locateFirst = current($allLocationsTemp[$key]);
                                $locationArr[$key][$locateFirst['id']] = $locateFirst;
                                $locationArr[$key][$locateFirst['city']] = [
                                    'id' => $locateFirst['city'],
                                ];
                                $locationArr[$key][$locateFirst['district']] = [
                                    'id' => $locateFirst['district'],
                                ];
                            }
                            else
                            {
                                // kiem tra tiep co nhieu du lieu
                                usort($allLocationsTemp[$key], function($a, $b) {
                                    if ($a['hit'] == $b['hit']) {
                                        return 0;
                                    }
                                    return ($a['hit'] > $b['hit']) ? -1 : 1;
                                });
                                $locateFirst = current(array_slice($allLocationsTemp[$key], 0, 1));
                                $locationArr[$key][$locateFirst['id']] = $locateFirst;
                                $locationArr[$key][$locateFirst['city']] = [
                                    'id' => $locateFirst['city'],
                                ];
                                $locationArr[$key][$locateFirst['district']] = [
                                    'id' => $locateFirst['district'],
                                ];
                            }
                        }
                    }
                }
            }
        }

        $locationReturn = [];
        if (count($locationArr) > 0) {
            foreach ($locationArr as $key =>  $local)
            {
                foreach ($local as $value)
                {
                    $selectFirstAddess = \DB::table('locations')->where('id', $value['id'])
                                        ->select('id','loc_level','loc_type','loc_name','loc_slug', 'loc_parent_id','loc_description')
                                        ->first();
                    if ($selectFirstAddess) $locationReturn[$key][] = $selectFirstAddess;
                }

                if (isset($locationReturn[$key]))
                {
                    // Sort cho dễ đọc
                    usort($locationReturn[$key], function($a, $b) {
                        if ($a->loc_level == $b->loc_level) {
                            return 0;
                        }
                        return ($a->loc_level > $b->loc_level) ? -1 : 1;
                    });
                }
            }
        }
        return $locationReturn;
    }


    /**
     * convert description mo ta trong location toi dia chi
     * @param $descriptionMore
     * @return array
     */
    public function convertValueStringOrArrayToSlug($descriptionMore)
    {
        $descriptionMoreRtn = [];
        if (is_string($descriptionMore))
        {
            $descriptionMore    = explode(',', $descriptionMore);
        }
        foreach ($descriptionMore as $description)
        {
            $descriptionMoreRtn[] = Str::slug($description);
        }
        return $descriptionMoreRtn;
    }

    /**
     * Remove một phần từ ra khỏi mảng
     * @param $key
     * @param array $needle
     * @return array
     */
    public static function removeItemFromArr($key, $needle = [])
    {
        $arrRtn = [];

        foreach ($needle as $index =>  $value)
        {
            if ($key != $value) $arrRtn[] = $value['name'];
        }
        return $arrRtn;
    }
    /**
     * Convert 1 mảng gồm nhiều value cách nhau bởi dấu phẩy về dạng slug
     * @param array $arr
     * @param int $positon
     * @return array|int
     */
    private function convertItemArrSlug($arr, $positon = '')
    {
        if (count($arr) > 0)
        {
            $arrItem = [];
            foreach ($arr as $key => $item)
            {
                if ($positon)
                {
                    list(, $district, $city) = explode(',', $item);
                    switch ($positon)
                    {
                        case 2:
                            $item = $district;
                            break;
                        case 3:
                            $item = $city;
                            break;
                    }
                }
                if ($item) $arrItem[$key] = implode(', ', $this->convertValueStringOrArrayToSlug($item));
            }
            return $arrItem;
        }
        return 0;
    }

    /**
     * B
     * @param $slug
     * @param array|integer $level
     * @return $this|\Illuminate\Support\Collection
     */
    public function getLocationBySlug($slug, $level)
    {
        $allLocations = DB::table('locations')->where('loc_slug', $slug);

        if (is_array($level))
        {
            $allLocations->whereIn('loc_level', $level);
        }else
        {
            $allLocations->where('loc_level',$level);
        }

        $allLocations = $allLocations->get();

        return $allLocations;
    }
}
