<?php

//include_once __DIR__.'/../app/simple_html_dom.php';

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (){return 1;});
Route::get('/crawl', 'crawlController@run');
Route::get('/crawlTestJob', 'crawlControllerTestJob@run');
//Route::get('/', 'likePageController@run');
Route::get('/test', 'likePageController@test');
Route::get('/analyze', 'keywordController@run');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
