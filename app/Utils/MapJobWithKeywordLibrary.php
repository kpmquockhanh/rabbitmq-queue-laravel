<?php
/**
 * Created by PhpStorm.
 * User: Hungokata
 * Date: 2/8/18
 * Time: 2:12 PM
 */

namespace App\Utils;

use Carbon\Carbon;
use App\Core123\CliEcho;
use App\Models\Jobs\JobAttribute;
use App\Models\Jobs\JobData;
use App\Models\Keywords\KeywordTemp;
use App\Utils\Keyword\KeywordLibrary as KeywordLibraryHelper;
use App\Models\Jobs\JobAttributeRepositoryInterface as JobAttributeRepository;
use App\Models\Keywords\KeywordLibraryRepositoryInterface as KeywordLibraryRepository;

class MapJobWithKeywordLibrary
{
    private $keywordLibraryHelper;
    private $keywordLibraryRepository;
    private $jobAttributeRepository;

    public function __construct(KeywordLibraryRepository $keywordLibraryRepository,
                                JobAttributeRepository $jobAttributeRepository)
    {
        $this->keywordLibraryHelper     = new KeywordLibraryHelper();
        $this->keywordLibraryRepository = $keywordLibraryRepository;
        $this->jobAttributeRepository   = $jobAttributeRepository;
    }

    /**
     * Map một job cùng với thư viện keyword
     * @param JobData $jobData
     */
    public function process(JobData $jobData, $debug = false)
    {
        $title    = $jobData->job_title;
        $jobId    = $jobData->id;
        if ($jobId)
        {
            $listKeywords = $this->keywordLibraryHelper->convertStrToArrayStandard($title);
            if (count($listKeywords) > 0)
            {
                foreach ($listKeywords as $keyword)
                {
                    CliEcho::infonl('[ JobID('.$jobId.') - '. $keyword . ' ]');

                    if ($keyword)
                    {
                        // Map tiêu đề với chức danh
                        $this->mạpJobWithAttributeTitle($jobId, $keyword);

                        // Map tiêu đề với từ khoá
                        $lengthKeyword = count(explode(' ', $keyword));
                        if ($lengthKeyword > 1)
                        {
                            try
                            {
                                $listKeywordResult = $this->keywordLibraryRepository->searchByJobKeyword($keyword,['ignore_type_company'=> 1], 100, false, 1);
                                if ($listKeywordResult->count())
                                {
                                    if ($debug == true) echo "<div style='background: #9da29d6e;padding: 2px 15px;'>";

                                    foreach ($listKeywordResult as $item)
                                    {
                                        try
                                        {
                                            $check = $this->keywordLibraryHelper->checkArrayDiffKeywordSearch($keyword, $item->kwl_name);

                                            if ($check === false)
                                            {
                                                CliEcho::infonl('- '. $item->kwl_name . ' ('. $item->id . ")");

                                                \DB::table('job_keyword_libraries')->insert([
                                                    'jkl_job_id' => $jobId,
                                                    'jkl_keyword_library_id' => $item->id
                                                ]);
                                            }
                                            else
                                            {
                                                $keywordSlug    = str_slug($keyword);
                                                $keywordExist   = \DB::table('keyword_libraries')
                                                                    ->where('kwl_slug', $keywordSlug)
                                                                    ->count();
                                                $keywordTempExist = \DB::table('keyword_temps')
                                                                    ->where('kwt_name_slug',$keywordSlug)
                                                                    ->count();

                                                if (!$keywordExist && !$keywordTempExist)
                                                {
                                                    \DB::table('keyword_temps')->insert([
                                                        'kwt_name'      => $keyword,
                                                        'kwt_name_slug' => $keywordSlug,
                                                        'kwt_status'    => KeywordTemp::STATUS_INIT,
                                                        'kwt_type'      => KeywordTemp::TYPE_JOB_TITLE,
                                                        'created_at'    => Carbon::now(),
                                                        'updated_at'    => Carbon::now()
                                                    ]);
                                                }
                                            }
                                        }catch (\Exception $e) {}
                                    }

                                    if ($debug) echo "</div>";
                                }
                            }catch (\Exception $e) {
                                \Log::warning('MapJobWithKeywordLibrary - process  - '. $e->getMessage());
                            }
                        }
                    }
                }
            }
        }
    }


    /**
     * Map cùng thuộc tính chức danh
     * @param $jobId
     * @param $keyword
     */
    private function mạpJobWithAttributeTitle($jobId, $keyword)
    {
        try
        {
            $arrTitle = $this->jobAttributeRepository->searchAttributeTitleByJobKeyword($keyword);
            if (count($arrTitle))
            {
                foreach ($arrTitle as $id => $name)
                {
                    try{
                        $check = $this->keywordLibraryHelper->checkArrayDiffKeywordSearch($keyword, $name);
                        if ($check === false)
                        {
                            \DB::table('job_datas_attributes')->insert([
                                'jda_attribute_id'    => $id,
                                'jda_job_id'          => $jobId,
                                'jda_type'            => JobAttribute::STATUS_TYPE_LEVEL
                            ]);
                        }
                    }catch (\Exception $e) {}
                }
            }

        }catch (\Exception $e){}
    }
}