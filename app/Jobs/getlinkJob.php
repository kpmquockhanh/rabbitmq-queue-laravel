<?php

namespace App\Jobs;

include_once __DIR__.'/../simple_html_dom.php';

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class getlinkJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $link;

    public function __construct($link)
    {
        $this->link = $link;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try
        {
            $data = [];

            $html = file_get_html($this->link);

        }
        catch (\Exception $e)
        {
//            echo "Sai link</br>";
//            die(1);
            $this->delete();
            $this->fail();
//            die(1);
        }


        foreach($html->find('.company-name') as $link)
        {
            $data['name'] = $link->plaintext;
        }

        foreach($html->find('.detail-info-company') as $link)
        {
            if (isset($link->find('b')[0]))
            {
//                echo $link->find('b')[0]->plaintext;

                if (preg_match('/Trụ sở:/',$link->find('b')[0]->plaintext))
                    $data['addr'] = $link->find('span')[0]->plaintext;
                if (preg_match('/Quy mô công ty:/',$link->find('b')[0]->plaintext))
                    $data['scale'] = $link->find('span')[0]->plaintext;
                if (preg_match('/Website:/',$link->find('b')[0]->plaintext))
                    $data['website'] = $link->find('span')[0]->plaintext;
            }
            else
                $data['desc'] = $link->outertext;
        }

        if (isset($html->find('.logo-employer > img')[0]->src))
            $data['img'] = 'https://viectotnhat.com'.$html->find('.logo-employer > img')[0]->src;

        if (isset($html->find('.employer-verified > img')[0]->src))
            $data['validated'] = true;
        else
            $data['validated'] = false;

//        echo $data['desc'];

        DB::table('viectotnhat')->insert([
            'name' => isset($data['name'])?$data['name']:'',
            'img' => isset($data['img'])?$data['img']:'',
            'addr' => isset($data['addr'])?$data['addr']:'',
            'scale' => isset($data['scale'])?$data['scale']:'',
            'website' => isset($data['website'])?$data['website']:'',
            'desc' => isset($data['desc'])?$data['desc']:'',
            'isValidated' => $data['validated'],
            'created_at' => date(now()),
        ]);
    }
}
