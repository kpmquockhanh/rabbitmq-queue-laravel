<?php
/**
 * Created by PhpStorm.
 * User: Hungokata
 * Date: 2/9/18
 * Time: 1:27 PM
 */

namespace App\Utils;

use App\Core123\Helper\StringHelper;

class MapDataHelper
{

    public static $intance = null;

    public static function getInstance()
    {
        if (self::$intance === null)
        {
            self::$intance = new self();
        }
        return self::$intance;
    }

    /**
     * Convert string to format date
     * @param string $date
     * @return false|null|string
     */
    public function convertToDate($date = '')
    {
        if ($date)
        {
            // remove các ký tự đặc biệt
            $date       = mb_strtolower($date);
            $strSpecial = array( chr(9), chr(10), chr(13), '(', ')', "'", "'", '%', '$', '#', '&', '^', "&quot;","&#34;","\"","&apos;","&#39;","'","&laquo;","&#171;","«","&raquo;","&#187;","»","?","“","”","(",")","!","[","]","{","}","|","\\","%","#","&","@","$","^","&","*","+","=",";","<",">","...","…");
            $textRemove = array('hạn nộp hồ sơ','hết hạn nộp','hạn nộp', 'hồ sơ');
            $date       = str_replace($strSpecial, "", $date);
            $date       = str_replace($textRemove, "", $date);
            $date       = trim(preg_replace('/^[\D]*/', '', $date));
            $newDate    = preg_replace("/(\d+)\D+(\d+)\D+(\d+)/","$3-$2-$1",$date);

            return date('Y-m-d', strtotime($newDate));
        }
        return NULL;
    }


    /**
     * Clear các ký tự
     * @param $str
     */
    public static function clearSalary($str = '')
    {
        $str = preg_replace('/\(.*\)?|:|\)?$/', '', $str);
        $str = preg_replace('(-|-)', " - ", $str);
        return trim($str);
    }

    /**
     * Convert lương từ 1 job hiển thị ra view
     * @param mixed $item
     * string
     */
    public function convertSalaryStandardToView($salaryDefault, $salaryMin=0, $salaryMax=0)
    {
        $salaryDefault = self::clearSalary($salaryDefault);
        if (strpos($salaryDefault, '$') !== false || strpos($salaryDefault, 'usd') !== false)
        {
            return $salaryDefault;
        }

        $prefix =  '';
        if (preg_match('/(dưới|trên)/', $salaryDefault, $match))
        {
            $prefix = ucfirst($match[0]) . ' ';
        }

        $suffix = ' Triệu';
        if (preg_match('/\d/', $salaryDefault));
        {
            $salary = '';
            if ($salaryMin)
            {
                $salary = $salaryMin >= 1000000 ?  $salaryMin / 1000000 : $salaryMin;
            }

            if ($salaryMax)
            {
                $salary .= ' - '. ($salaryMax > 1000000 ? $salaryMax /1000000 : $salaryMax );
            }

            if ($salary)
            {
                return $prefix . $salary . $suffix;
            }
        }

        return mb_ucwords($salaryDefault);
    }

    /**
     * Convert tới khoảng giá cho lương
     * @param $str
     * @return array|string
     */
    public static function convertToSalaryRange($str)
    {
        if (!$str) return 0;

        $str = StringHelper::convertStrToLower($str);
        $str = self::clearSalary($str);

        // Xac dinh don vi tien te
        if (strpos($str, 'trieu') !== false)
        {
            $arrNewSalary = [
                'unit'  => 'trieu',
                'value' => $str
            ];
        }else if (strpos($str, '$') !== false || strpos($str, 'usd') !== false)
        {
            $arrNewSalary = [
                'unit'  => 'usd',
                'value' => $str
            ];
        }else if (strpos($str, 'vnd') !== false)
        {
            $arrNewSalary = [
                'unit'  => 'vnd',
                'value' => $str
            ];
        }else
        {
            $arrNewSalary = [
                'unit'  => null,
                'value' => $str
            ];
        }

        $arrNewSalary['value'] = str_replace(['.', ','], '', rtrim(trim($str), ','));

        // Get range price
        preg_match_all('/([0-9\.,]+)/', $arrNewSalary['value'], $match);
        if (count($match) > 0) $arrNewSalary['price_current'] = $match[0];

        if (isset($arrNewSalary['value']))
        {
            $oneDolar     = 22700;
            $million      = 1000000;
            $minPrice     = 0;
            $maxPrice     = 0;
            switch ($arrNewSalary['unit'])
            {
                case 'trieu':
                    $minPrice = isset($arrNewSalary['price_current'][0]) ? $arrNewSalary['price_current'][0] * $million : 0;
                    $maxPrice = isset($arrNewSalary['price_current'][1]) ? $arrNewSalary['price_current'][1] * $million : 0;
                    break;

                case 'vnd':
                    $minPrice = isset($arrNewSalary['price_current'][0]) ? $arrNewSalary['price_current'][0] : 0;
                    $maxPrice = isset($arrNewSalary['price_current'][1]) ? $arrNewSalary['price_current'][1] : 0;

                    if ($minPrice && strlen($minPrice) < 3) $minPrice = $minPrice * $million;
                    if ($maxPrice && strlen($maxPrice) < 3) $maxPrice = $maxPrice * $million;
                    break;

                case 'usd':
                    if (isset($arrNewSalary['price_current'][0]))
                    {
                        $minPrice = (int)substr($arrNewSalary['price_current'][0], 0, 4) * $oneDolar;
                        if ($arrNewSalary['price_current'][0] > 4)
                        {
                            if (strlen($minPrice) > 8) $minPrice = substr($minPrice, 0, 8);
                        }
                    }

                    if (isset($arrNewSalary['price_current'][1]))
                    {
                        $maxPrice = (int)substr($arrNewSalary['price_current'][1], 0, 4) * $oneDolar;
                        if ($arrNewSalary['price_current'][0] > 4)
                        {
                            if (strlen($maxPrice) > 8) $maxPrice = substr($maxPrice, 0, 8);
                        }
                    }

                    break;

                default:
                    $minPrice = isset($arrNewSalary['price_current'][0]) ? $arrNewSalary['price_current'][0] : 0;
                    $maxPrice = isset($arrNewSalary['price_current'][1]) ? $arrNewSalary['price_current'][1] : 0;

                    if (strlen($minPrice) > 8) $minPrice = substr($minPrice, 0, 8);
                    if (strlen($maxPrice) > 8) $maxPrice =  substr($maxPrice, 0, 8);

                    break;
            }

            $arrNewSalary['min_salary'] = (int)$minPrice;
            $arrNewSalary['max_salary'] = (int)$maxPrice;

            return $arrNewSalary;
        }

        return 0;
    }
}