<?php namespace App\Core123\File\Uploads;

use App\Core123\File\Uploads\Exceptions\UploadFolderDoesNotExistException;

class Uploader {

    public $key;

    public function __construct(Upload $upload)
    {
        $this->upload = $upload;
    }

    public function upload($fileControl)
    {
        $this->upload->key = $this->key;
        $pathUpload = $this->getUploadFolderPathToDay();
        return $this->upload->upload($fileControl, $pathUpload);
    }

    public function uploadFromUrl($url)
    {
        $pathUpload = $this->getUploadFolderPathToDay();
        return $this->upload->uploadFromUrl($url, $pathUpload);
    }

    public function uploadMulti($fileControl)
    {
        $pathUpload = $this->getUploadFolderPathToDay();
        return $this->upload->uploadMulti($fileControl, $pathUpload);
    }

    /**
     * Trả về dạng đầy đủ thông tin upload
     * @param $fileControl
     * @return array
     */
    public function uploadMultiV2($fileControl)
    {
        $pathUpload = $this->getUploadFolderPathToDay();
        return $this->upload->uploadMultiV2($fileControl, $pathUpload);
    }

    public function getUploadFolderPathToDay() {
        $pathUpload = $this->upload->getUploadFolderPath().'/'. date('Y').'/'.date('m').'/'.date('d');

        // Create folder if it not exist
        if(!is_dir($pathUpload)) {
            try {
                @mkdir($pathUpload, 0755, true);
                @chmod($pathUpload, 0777);
            } catch (\ErrorException $e) {
                throw new UploadFolderDoesNotExistException("Upload folder does not exist or you need chmod 777 to this folder", 1);
            }
        }

        return $pathUpload;
    }

}