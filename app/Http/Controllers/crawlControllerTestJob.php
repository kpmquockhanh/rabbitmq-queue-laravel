<?php

namespace App\Http\Controllers;

use App\Jobs\bookingJob;
use App\Jobs\chudu24;
use App\Jobs\getlinkJob;
use App\Jobs\getlinkJobTimViecNhanh;
use App\Jobs\kenhnhansuJob;
use App\Jobs\linkKeyword;
use App\Jobs\topcvJob;
use App\Jobs\tuyencongnhanjJob;
use App\Jobs\viecITjob;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use Openbuildings\Spiderling\Page;
use App\Utils\Keyword\KeywordLibrary;

include_once __DIR__.'/../../simple_html_dom.php';

class crawlControllerTestJob extends Controller
{
    //

    public function run()
    {
//        $links = [
//            '','d-f','g-i','j-k','m-o','p-r','s-v','w-z',
//        ];
//        foreach ($links as $link)
//        {
//            $html = new \simple_html_dom($this->curl('https://itviec.com/viec-lam-it-theo-ten-cong-ty/'.$link,false));
//            foreach ($html->find('li.skill-tag__item a') as $item)
//            {
//                echo 'https://itviec.com'.$item->href.'</br>';
//                dispatch(new viecITjob('https://itviec.com'.$item->href));
//            }
//        }
//        $html = new \simple_html_dom($this->curl('https://itviec.com/nha-tuyen-dung/active-media-vietnam',false));
//
//
//        $banner = ($html->find('.company-page .cover-images-cropped img',0)->src);
//
//        $logo = ($html->find('.headers .logo-container .logo img',0)->src);
//
//        $name = ($html->find('.headers .name-and-info h1',0)->plaintext);
//
//        $location = ($html->find('.headers .name-and-info span',0)->plaintext);
//
//        $type = ($html->find('.headers .name-and-info .company-info .gear-icon',0)->plaintext);
//
//        $scale = ($html->find('.headers .name-and-info .company-info .group-icon',0)->plaintext);
//
//        $country = ($html->find('.headers .name-and-info .company-info .country',0)->plaintext);
//
//        $workingDate = ($html->find('.headers .name-and-info .working-date',0)->plaintext);
//
//        $overTime = ($html->find('.headers .name-and-info .overtime',0)->plaintext);
//
//        $des = ($html->find('.company-container .col-left .panel-default',0)->outertext);
//
//        DB::table('viecIT')->insert([
//           'name' => $name,
//           'img' => $logo,
//           'banner' => $banner,
//           'location' => $location,
//           'type' => $type,
//           'scale' => $scale,
//           'name' => $name,
//           'country' => $country,
//           'workingdate' => $workingDate,
//           'OT' => $overTime,
//           'desc' => $des,
//           'link' => 'https://itviec.com/nha-tuyen-dung/allgrowlabo-co-ltd',
//        ]);







//        dd('https://itviec.com'.$html->find('li.skill-tag__item a',0)->href);




        $html = new \simple_html_dom($this->curl('https://tuyencongnhan.vn/nha-tuyen-dung.xml',false));
        $items = $html->find('loc');

        foreach ($items as $item)
        {
            echo $item->plaintext.'<br>';

            $this->dispatch( new tuyencongnhanjJob(($item->plaintext)));
        }

//        $img = 'https://tuyencongnhan.vn'.($html->find('.employer-info img.logo-employer',0)->src);
//        !strpos($img,'no-image.png')?:$img='';
//
//        $des = html_entity_decode($html->find('.employer-info p.description',0)->plaintext);
//        $scale = $html->find('.employer-info p',1)->plaintext;
//        $scale = str_replace(' ','', $scale);
//        $scale = str_replace('Quymô:','', $scale);
//
//        $address = $html->find('address.employer-info',0);
//
//        $name ='';
//        $addr ='';
//        $phone ='';
//        $web ='';
//
//        foreach($address->find('p') as $item)
//        {
//            if ($hi = preg_match('/Tên đầy đủ : /',$item->plaintext))
//                $name = str_replace('Tên đầy đủ : ','', $item->plaintext);
//            elseif ($hi = preg_match('/Địa chỉ : /',$item->plaintext))
//                $addr = str_replace('Địa chỉ : ','', $item->plaintext);
//            elseif ($hi = preg_match('/Số điện thoại : /',$item->plaintext))
//                $phone = str_replace('Số điện thoại : ','', $item->plaintext);
//            elseif ($hi = preg_match('/Website : /',$item->plaintext))
//                $web = str_replace('Website : ','', $item->plaintext);
//        }
//
//
//        DB::table('tuyencongnhan')->insert([
//            'name' => $name,
//            'img' => $img,
//            'addr' => $addr,
//            'scale' => $scale,
//            'website' => $web,
//            'desc' => $des,
//            'phone' => $phone,
//
//        ]);
    }

    public function tachchuoi($str)
    {
        return preg_replace('/ {2,}/','',$str);
    }


    function curl($url,$header)
    {
        $data = curl_init();
        curl_setopt($data, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($data, CURLOPT_URL, $url);
        curl_setopt($data, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($data, CURLOPT_HEADER  , $header);
        curl_setopt($data, CURLOPT_ENCODING,       'gzip,deflate'  );
        curl_setopt($data, CURLOPT_COOKIEJAR, 'tmp/cookies.txt');
        curl_setopt($data, CURLOPT_COOKIEFILE, 'tmp/cookies.txt');
        curl_setopt($data, CURLOPT_SSL_VERIFYPEER, FALSE );
        curl_setopt($data,CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36');
//        curl_setopt($data,CURLOPT_USERAGENT,'AdsBot-Google (+http://www.google.com/adsbot.html)');
        $result = curl_exec($data);
        curl_close($data);
        return $result;
    }
    function curlPost($url,$header, $info)
    {

        $data = curl_init();
        curl_setopt($data, CURLOPT_REFERER, 'https://www.tripadvisor.com.vn/Hotels-g298082-Hoi_An_Quang_Nam_Province-Hotels.html');
        curl_setopt($data, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($data, CURLOPT_URL, $url);
        curl_setopt($data, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($data, CURLOPT_COOKIEJAR, 'tmp/cookies.txt');
        curl_setopt($data, CURLOPT_COOKIEFILE, 'tmp/cookies.txt');
        curl_setopt($data, CURLOPT_HEADER  , $header);
        curl_setopt($data, CURLOPT_ENCODING,       'gzip,deflate'  );
        curl_setopt($data, CURLOPT_SSL_VERIFYPEER, FALSE );
        curl_setopt($data,CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36');
        curl_setopt($data, CURLOPT_POSTFIELDS, http_build_query($info));
        $result = curl_exec($data);
        curl_close($data);
        return $result;
    }

    public function getViaLink($start, $end)
    {

        $context = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
        for ($i = $start; $i <= $end; $i++) {
            //Load từ link
            $url = "https://www.timviecnhanh.com/sitemap/sitemap-employer-".$i.".xml";
            $xml = file_get_contents($url, false, $context);
            $xml = simplexml_load_string($xml);

            //Hàm dùng chung
            $x = 1;
            foreach ($xml->children() as $item) {
                //Get từ link
                //dump(trim(str_replace('\n', '', (string)$item->loc)));

//                \DB::table('viectotnhat_link')->insert([
//                    'link' => trim(str_replace('\n', '', (string)$item->loc)),
//                ]);
                dispatch(new getlinkJobTimViecNhanh((trim(str_replace('\n', '', (string)$item->loc)))));
                echo trim(str_replace('\n', '', (string)$item->loc)).'</br>';
                echo "Insert success " . $x . "</br>";
                $x++;
            }
            echo "-----------------Lay xong link trang xml " . $i . "</br>";
        }
    }
}
