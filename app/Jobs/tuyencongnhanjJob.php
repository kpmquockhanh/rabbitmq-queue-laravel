<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

include_once __DIR__.'/../simple_html_dom.php';

class tuyencongnhanjJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $link;
    public function __construct($link)
    {
        $this->link = $link;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $html = new \simple_html_dom($this->curl($this->link,false));

            $img = 'https://tuyencongnhan.vn'.($html->find('.employer-info img.logo-employer',0)->src);
            !strpos($img,'no-image.png')?:$img='';

            $des = html_entity_decode($html->find('.employer-info p.description',0)->plaintext);
            $scale = $html->find('.employer-info p',1)->plaintext;
            $scale = str_replace(' ','', $scale);
            $scale = str_replace('Quymô:','', $scale);

            $address = $html->find('address.employer-info',0);

            $name ='';
            $addr ='';
            $phone ='';
            $web ='';

            foreach($address->find('p') as $item)
            {
                if ($hi = preg_match('/Tên đầy đủ : /',$item->plaintext))
                    $name = str_replace('Tên đầy đủ : ','', $item->plaintext);
                elseif ($hi = preg_match('/Địa chỉ : /',$item->plaintext))
                    $addr = str_replace('Địa chỉ : ','', $item->plaintext);
                elseif ($hi = preg_match('/Số điện thoại : /',$item->plaintext))
                    $phone = str_replace('Số điện thoại : ','', $item->plaintext);
                elseif ($hi = preg_match('/Website : /',$item->plaintext))
                    $web = str_replace('Website : ','', $item->plaintext);
            }


            DB::table('tuyencongnhan')->insert([
                'name' => $name,
                'img' => $img,
                'addr' => $addr,
                'scale' => $scale,
                'website' => $web,
                'desc' => $des,
                'phone' => $phone,
                'link' => $this->link,

            ]);
        }
        catch(\Exception $e)
        {
            echo $e->getMessage();
        }
    }
    function curl($url,$header)
    {
        $data = curl_init();
        curl_setopt($data, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($data, CURLOPT_URL, $url);
        curl_setopt($data, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($data, CURLOPT_HEADER  , $header);
        curl_setopt($data, CURLOPT_ENCODING,       'gzip,deflate'  );
        curl_setopt($data, CURLOPT_COOKIEJAR, 'tmp/cookies.txt');
        curl_setopt($data, CURLOPT_COOKIEFILE, 'tmp/cookies.txt');
        curl_setopt($data, CURLOPT_SSL_VERIFYPEER, FALSE );
        curl_setopt($data,CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36');
//        curl_setopt($data,CURLOPT_USERAGENT,'AdsBot-Google (+http://www.google.com/adsbot.html)');
        $result = curl_exec($data);
        curl_close($data);
        return $result;
    }
}
