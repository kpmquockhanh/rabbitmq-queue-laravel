<?php
/**
 * Created by PhpStorm.
 * User: Hungokata
 * Date: 1/3/18
 * Time: 10:47 PM
 */

// Register a singleton Upload ||  Register a singleton ImageFactory
$app->singleton('Uploader', function() {
    return new \App\Core123\File\Uploads\Uploader(new \App\Core123\File\Uploads\Upload());
});

$app->singleton('ImageUploader', function() {
    $uploader = App::make('Uploader');
    $image    = App::make('App\Core123\File\Images\Image');
    return new \App\Core123\File\Images\ImageFactoryV2($uploader, $image);
});

// ++++++++++++++++++++++++++++++++++++++++++++++ Cách sử dụng +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$upload = App::make('ImageUploader');
try
{
    $upload->upload('picture', '' , 'resize');
}catch (Exception $e)
{
    $errors = $e->getMessage();
}