<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class keywordJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

//    private $id;
    private $title;
    private  $keyword;
    public function __construct($title, $keyword)
    {
//        $this->id = $id;
        $this->keyword = $keyword;
        $this->title = $title;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::table('title_keyword')->insert([
           'title' => $this->title,
            'keyword' => $this->keyword,
        ]);
//        echo $this->title;
    }
}
