<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class kenhnhansuJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $chucdanh;
    private $congty;
    private $luong;
    private $diadiem;
    private $thoigian;
    public function __construct($chucdanh, $congty, $luong, $diadiem, $thoigian)
    {
        $this->chucdanh = $chucdanh;
        $this->congty = $congty;
        $this->luong = $luong;
        $this->diadiem = $diadiem;
        $this->thoigian = $thoigian;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $time = strtotime($this->thoigian);

        $newformat = date('Y-m-d',$time);

        DB::table('kenhnhansu')->insert([
           'chucdanh' => $this->chucdanh,
           'congty' => $this->congty,
           'luong' => $this->luong,
           'diadiem' => $this->diadiem,
            'thoigian' => $newformat,
        ]);
    }
}
