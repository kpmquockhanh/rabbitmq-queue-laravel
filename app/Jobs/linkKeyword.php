<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

include_once __DIR__."/../simple_html_dom.php";

class linkKeyword implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $link;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($link)
    {
        $this->link = $link;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        echo $this->link."\n";
        $html = file_get_html($this->link);
        foreach($html->find('h1') as $link)
            DB::table('title_keyword')->insert([
               'title' => html_entity_decode($link->plaintext),
                'keyword' => '',
                'created_at' => date(now())
            ]);
    }
}
