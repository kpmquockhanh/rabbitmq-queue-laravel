<?php

namespace App\Core123\Api\Test;

use App\Core123\Api\Test\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class ProductController
 * @package App\Http\Controllers
 *  Ý nghĩa của mỗi response status trong HTTP:
 *  200: Ok. Mã cơ bản có ý nghĩa là thành công trong hoạt động.
201: Đối tượng được tạo, được dùng trong hàm store.
204: Không có nội dung trả về. Hoàn thành hoạt động nhưng sẽ không trả về nội dung gì.
206: Trả lại một phần nội dung, dùng khi sử dụng phân trang.
400: Lỗi. Đây là lỗi cơ bản khi không vượt qua được xác nhận yêu cầu từ server.
401: Unauthorized. Lỗi do yêu cầu authen.
403: Forbidden. Lõi này người dùng vượt qua authen, nhưng không có quyền truy cập.
404: Not found. Không tìm thấy yêu cầu tương tứng.
500: Internal server error.
503: Service unavailable.
 */

class ProductController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try
        {
            $statusCode = 200;
            $response = [
                'data' => []
            ];

            $products = Product::with([
                'productMeta'=> function($query)
                {
                    $query->select('prme_product_id', 'prme_teaser', 'prme_spec_json');
                }
            ])->limit(20)->orderBy('pro_id', 'ASC')->get();

            if ($products->count())
            {
                foreach($products as $product)
                {
                    $response['data'][] = [
                        'id'             => $product->pro_id,
                        'name'           => $product->pro_name,
                        'keywords'       => $product->pro_keywords,
                        'price'          => $product->pro_price,
                        'price_min'      => $product->pro_min_price,
                        'created_at'     => $product->pro_created_at,
                        'updated_at'     => $product->pro_updated_at,
                        'product_meta'   => $product->productMeta
                    ];
                }
            }
        }catch (\Exception $e)
        {
            $statusCode = 400;
        }finally
        {
            return response()->json($response, $statusCode);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::create($request->all());

        return response()->json($product, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $product = Product::with('productMeta')->where('pro_id', $id)->first();
            $statusCode = 200;
            $response = [ "data" => [
                'name'           => $product->pro_id,
                'keywords'       => $product->pro_keywords,
                'price'          => $product->pro_price,
                'price_min'      => $product->pro_min_price,
                'created_at'     => $product->pro_created_at,
                'updated_at'     => $product->pro_updated_at,
                'product_meta'   => $product->productMeta
            ]];

        }catch(\Exception $e){
            $response = [
                "error" => "Record is not found"
            ];
            $statusCode = 404;
        }finally
        {
            return response()->json($response, $statusCode);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product->update($request->all());

        return response()->json($article, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return response()->json(null, 204);
    }
}
