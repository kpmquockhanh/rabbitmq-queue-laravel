<?php
/**
 * Created by PhpStorm.
 * User: Hungokata
 * Date: 4/15/18
 * Time: 9:17 AM
 */

namespace App\Core123\Traits;


trait StatisticBase
{
    /**
     * Thống kê thông tin
     * @param array $filter
     */
    public function statisticByFilter($table, $filter = [])
    {
        $whereRawline = [
            'Hôm nay'    => [
                'type'   => 1,
                'whereRaw'  => 'Day(created_at) = Day(Now())',
                'label'  => 'Today'
            ],
            'Hôm qua'    => [
                'type'   => 2,
                'whereRaw'  => 'DATE(created_at) = DATE(DATE_SUB(CURDATE(), INTERVAL 1 DAY))',
                'label'  => 'Yesterday',
            ],
            'Tuần qua'   => [
                'type'   => 1,
                'whereRaw'  => 'DATE_SUB(CURDATE(), INTERVAL 7 DAY) <= DATE(created_at)',
                'label'  => 'Week'
            ],
            'Tháng qua'  => [
                'type'   => 1,
                'whereRaw'  => 'Month(created_at) = Month(Now())',
                'label'  => 'Month'
            ],
//            'Năm nay'  => [
//                'type'   => 1,
//                'whereRaw'  => 'Year(created_at) = Year(Now())',
//                'label'  => 'Year'
//            ]
        ];

        $statistic = [];
        foreach ($whereRawline as $item)
        {
            $query = \DB::table($table)->whereRaw($item['whereRaw']);
            if (is_callable(array_get($filter, 'callback')))
            {
                $query->where(array_get($filter, 'callback'));
            }
            $statistic[$item['label']] = $query->count();
        }
        return $statistic;
    }


}