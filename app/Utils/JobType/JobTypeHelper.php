<?php
namespace App\Utils\JobType;
class JobTypeHelper
{
    private $arrJobTypeDefault = [
        'toàn thời gian cố định' =>[
            'toàn thời gian cố định',
            'giờ hành chính',
            'full-time',
        ],
        'toàn thời gian tạm thời' => [
            'toàn thời gian tạm thời'
        ],
        'bán thời gian cố định' => [
            'bán thời gian cố định',
            'bán thời gian'
        ],
        'bán thời gian tạm thời' => [
            'bán thời gian tạm thời',
            'làm theo ca',
            'làm thêm ngoài giờ',
            'part-time',
        ],
        'theo hợp đồng/tư vấn' => [
            'theo hợp đồng/tư vấn',
            'thỏa thuận tùy theo năng lực',
            'thời gian thử việc thỏa thuận',
            'làm việc luôn',
            'nhận việc luôn',
            'hợp đồng',
            '1 năm'
        ],
        'thực tập' => [
            'thực tập',
            'thương lượng',
            'thoả thuận'
        ]
    ];
    private $replateType =
    [
        'months' => 'tháng'
    ];


    /**
     * Get type job
     * @param string $string
     * @return array
     */
    public static function getJobType($string)
    {
        $self          = new self();
        $resultJobType = array();
        $string        = str_replace(array_keys($self->replateType),array_values($self->replateType),$string);
        if( $string )
        {
            $arrJobType = $self->tachJobType($string);
            if( count($arrJobType) > 0 )
            {
                foreach ($arrJobType as $key => $type )
                {
                    $type = trim($type);
                    if ($type)
                    {
                        $arrJobTypeDefault = $self->arrJobTypeDefault;
                        foreach($arrJobTypeDefault as $index => $jobcheck)
                        {
                            if(in_array($type,$jobcheck))
                            {
                                $resultJobType[$key] = [
                                    'jobType'  => $index,
                                    'nameType' => $type
                                ];
                                break;
                            }
                            $resultJobType[$key] = [
                                'jobType'  => null,
                                'nameType' => $type
                            ];
                        }
                    }
                }
            }
        }

        // check truong hop //01 tháng || 1-3 tháng || 2 đến 3 tháng || 2 months || 3-6 tháng || từ 01 đến 02 tháng || trong vòng 2 tháng|| 45 ngày
        if( ! empty($resultJobType))
        {
            foreach($resultJobType as $key => $type)
            {
                if ( ! $type['jobType'])
                {
                    $pattern6 = '/^(.*)(([0-9]-[0-9])|[0-9])(.+)(tháng|ngày)$/';
                    if(preg_match_all($pattern6,$type['nameType'],$match))
                    {
                        $resultJobType[$key] = [
                            'jobtype'  => 'thực tập',
                            'nameType' => $type['nameType']
                        ];
                        continue;
                    }

                    $pattern11 = '/^(.*)(\d)h(.*)-(.*)(\d)h/';
                    $pattern12 = '/^(.*)(\d)h(.*)(\d)h(\d){1,2}/';
                    $pattern13 = '/(.*)(\d){1,2}()(.*)[a-z](.*)[a-z]|(.*)(\d){1,2}()(.*)[a-z](.*)[a-z]((,)(.*)(\d){1,2}()(.*)[a-z](.*)[a-z])/';
                    if(preg_match_all($pattern11,$type['nameType'],$match)||preg_match_all($pattern12,$type['nameType'],$match) || preg_match_all($pattern13,$type['nameType'],$match))
                    {
                        $resultJobType[$key] = [
                            'jobtype'  => 'toàn thời gian cố định',
                            'nameType' => $type['nameType']
                        ];
                        continue;
                    }
                }
            }
        }

        return $resultJobType;
    }

    public static function tachJobType($string)
    {
        $string = mb_strtolower($string, 'UTF-8');
        $arrJobType = preg_split("/;|,/",$string);
        return $arrJobType ;
    }
}