<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;


class chudu24 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $link;
    public function __construct($link)
    {
        $this->link = $link;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Tên khách sạn , resort
        //Giá ( nếu có )
        //Địa chỉ
        //Số điện thoại
        //Email
        //Mô tả
        //Avatar
        //Hình ảnh liên quan
        //Số sao
        //Mô tả ( trang chi tiết )
        //Link ( chi tiết khách sạn đó )
        //Type ( 1: khách sạn, 2: resort , vvv..)


        try
        {
            $data = [];
            $html = new \simple_html_dom($this->curl($this->link,false));

            $data['name'] = $html->find('.title a#lnkhotelname span')[0]->plaintext;

            $matches = [];

            $data['star'] = (float)$html->find('.hoteltop-right span span')[0]->plaintext;

            $data['voteNum'] = $html->find('.caption')[0]->plaintext;

            $data['address'] = $html->find('.address p')[0]->plaintext;

            $data['benefit'] = '';
            foreach($html->find('.listsv span') as $item)
                $data['benefit'] .=$item->plaintext.'|';

            $a = [];
            $data['img'] = '';
            foreach($html->find('.rsTmb') as $img)
                array_push($a, $img->src);
            $a = array_unique($a);

            foreach ($a as $img)
                $data['img'] .= $img.'|';

            $data['desc'] = $html->find('.pHotelDes')[0]->outertext;


            $data['location'] = $html->find('#maplocation .divLeft')[0]->outertext;

            $data['facilities'] = '';
            foreach ($html->find('.HotelFacilities .hotel-faci .col span') as $item)
                $data['facilities'] .= $item->plaintext.'|';


            DB::table('chudu24')->insert([
                'name' => $data['name'],
                'star'	=> $data['star'],
                'voteNum' => $data['voteNum'],
                'address' => $data['address'],
                'benefit' => $data['benefit'],
                'img' => $data['img'],
                'desc' => $data['desc'],
                'location' => $data['location'],
                'facilities' => $data['facilities'],
                'link' => $this->link,
            ]);
        }
        catch(\Exception $e)
        {
            echo $e->getMessage();
        }

    }
    function curl($url,$header)
    {
        $data = curl_init();
        curl_setopt($data, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($data, CURLOPT_URL, $url);
        curl_setopt($data, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($data, CURLOPT_HEADER  , $header);
        $result = curl_exec($data);
        curl_close($data);
        return $result;
    }
}
