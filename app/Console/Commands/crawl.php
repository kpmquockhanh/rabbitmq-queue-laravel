<?php

namespace App\Console\Commands;

use App\Utils\Keyword\KeywordLibrary;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class crawl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:run {to} {from}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 5000);
        $to = $this->argument('to');
        $from = $this->argument('from');
        $keywordFilter = new KeywordLibrary();
        for ($i=$to;$i<=$from;$i++)
        {
            echo "Page $i\n";
            $page = new \simple_html_dom($this->curl('https://www.jobstreet.vn/j?l=&p='.$i,false));
            foreach($page->find('.jwrap .job h2 a') as $job)
            {
                $filted = $keywordFilter->convertStrToArrayStandard($job->title);
                $keywords = '';
                foreach ($filted as $fil)
                    if ($fil === end($filted))
                        $keywords .= $fil;
                    else
                        $keywords .= $fil.'|';


                DB::table('jobstreet')->insert([
                    'title' => $job->title,
                    'keyword' => $keywords,
                ]);
            }
        }
        echo "DONE!\n";

    }
    function curl($url,$header)
    {
        $data = curl_init();
        curl_setopt($data, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($data, CURLOPT_URL, $url);
        curl_setopt($data, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($data, CURLOPT_HEADER  , $header);
        curl_setopt($data, CURLOPT_ENCODING,       'gzip,deflate'  );
        curl_setopt($data, CURLOPT_COOKIEJAR, 'tmp/cookies.txt');
        curl_setopt($data, CURLOPT_COOKIEFILE, 'tmp/cookies.txt');
        curl_setopt($data, CURLOPT_SSL_VERIFYPEER, FALSE );
        curl_setopt($data,CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36');
//        curl_setopt($data,CURLOPT_USERAGENT,'AdsBot-Google (+http://www.google.com/adsbot.html)');
        $result = curl_exec($data);
        curl_close($data);
        return $result;
    }
}
