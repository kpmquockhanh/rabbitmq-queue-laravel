<?php

namespace App\Jobs;

include_once __DIR__.'/../simple_html_dom.php';

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class getlinkJobTimViecNhanh implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $link;

    public function __construct($link)
    {
        $this->link = $link;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try
        {
            $data = [];
//            $html = file_get_html($this->link);
            $html = new \simple_html_dom($this->curl($this->link));

            $data['title'] = ($this->tachchuoi($html->find('.title > .text')[0]->plaintext));

            foreach($html->find('.summay-company > p') as $key => $item)
                switch ($key)
                {
                    case 0:  $data['addr'] = preg_replace('/<b>.*?> */', '', $item->innertext); break;
                    case 1:  $data['website'] = preg_replace('/<b>.*?> */', '', $item->innertext); break;

                    case 2: $data['act'] = str_replace('Lĩnh vực hoạt động: ','',$item->plaintext);

                }

            $data['img'] = $html->find('.block-content > img')[0]->src;
            if(isset($html->find('.block-content > img')[1]))
                $data['verified'] = strpos($html->find('.block-content > img')[1]->src,'no-ok') ? false : true;
            else
                $data['verified'] = false;
//        var_dump($data);
//            dd($data);
            \DB::table('timviecnhanh')->insert([
                'title' => isset($data['title'])?$data['title']:'',
                'img' => isset($data['img'])?$data['img']:'',
                'addr' => isset($data['addr'])?$data['addr']:'',
                'website' => isset($data['website'])?$data['website']:'',
                'act' => isset($data['act'])?$data['act']:'',
                'verified' => $data['verified'],
            ]);
        }
        catch(\Exception $e)
        {
            $this->delete();
            $this->fail();
//            echo 'sai';
//            die(1);
        }
    }
    public function tachchuoi($str)
    {
        return preg_replace('/ {2,}/','',$str);
    }
    function curl($url)
    {
        $data = curl_init();
        curl_setopt($data, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($data, CURLOPT_URL, $url);
        $result = curl_exec($data);
        curl_close($data);
        return $result;
    }
}
