<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 4/15/18
 * Time: 10:15 PM
 */

namespace App\Utils;
class MetaSeoTool
{
    protected $options = array();
    protected $title ;
    protected $metaDescription ;
    protected $metaKeyword;
    protected $image ;
    protected $sizeImage;
    protected $webpage ;
    protected $metaDataDefault = [
        'image' => 'https://123job.vn/images/social.png'
    ];
    protected $detail_page ;

    private $typeSeo           = 0; // mặc định việc làm
    private $language          = 'vi';
    private $prefixKeyword     = 'Việc làm ';
    private $prefixLocation    = 'Tìm việc làm nhanh ';
    private $suffixKeyword     = ' tuyển dụng ';
    private $separatorLocation = ' tại ';
    private $widthImage        = '600';
    private $heightImage       = '315';

    /**
     * @param $options
     *   | name_page (tên trang ) , webpage (key trong file cau hinh ) | title_page, description_page
     * @return void
     */
    public static function setMetaSeo($options)
    {
        $that = new self();
        $that->options = $options;
        $that->resetPrefix();
        $that->setLanguage();
        $that->setWebPage();
        $that->setDetailPage();
        $that->setMetaDataDefault();
        $that->setMetaTitle();
        $that->setMetaDescription();
        $that->setMetakeyword();
        $that->setImage();

        return $that;
    }

    /**
     * Reset khi không phải là trang việc làm
     */
    private function resetPrefix()
    {
        $type = array_get($this->options, 'type_seo', 0);
        if ($type != $this->typeSeo)
        {
            $this->prefixKeyword     = '';
            $this->prefixLocation    = '';
            $this->separatorLocation = '';
        }
    }

    private function setLanguage()
    {
        $this->language = array_get($this->options, 'language', 'vi');
    }

    private function setWebpage()
    {
        $this->webpage = array_get($this->options,'webpage');
    }

    private function setMetaDataDefault()
    {
        $this->metaDataDefault = $this->webpage ? config('job_global')['meta'][$this->language][$this->webpage] : [];
    }

    private function setDetailPage()
    {
        $this->detail_page = array_get($this->options,'detail_page');
    }


    private function convertKeywordSpecial($keyword)
    {
        $keyword = str_replace(['Tnhh', 'Mtv'], ['TNHH', 'MTV'], $keyword);
        return $keyword;
    }

    /**
     * Set meta title
     */
    private function setMetaTitle()
    {
        $location           = array_get($this->options,'location');
        $title_page         = array_get($this->options,'title_page');
        $keyword_search     = array_get($this->options,'keyword_search');
        $total              = array_get($this->options,'total');
        $keyword_is_company = array_get($this->options, 'keyword_is_company', 0);

        if ($keyword_is_company)
        {
            $this->prefixLocation = '';
            $keyword_search = mb_ucwords($keyword_search);
            $keyword_search = $this->convertKeywordSpecial($keyword_search);
        }

        $title = '';
        if ($keyword_search)
        {

            $title .= $this->prefixLocation . $keyword_search;

            if ($keyword_is_company)
            {
                $title .= $this->suffixKeyword;
            }
            $flag = 1;
        }

        if ($location)
        {
            $title .=  (!isset($flag) ? $this->prefixLocation.$this->separatorLocation : $this->separatorLocation) .$location;
            $flag = 2;
        }

        if ($total) $title .= (isset($flag)  ?    ' - ' : ' Có tất cả ') .$total . ' việc - ' .date('m/Y');

        if( isset($this->metaDataDefault['title']))
        {
            if ( !$location && !$keyword_search)
            {
                $this->title  = (!empty($title) ? $title . ' | ' :  $title) .$this->metaDataDefault['title'] ;

            }else
            {
                $this->title  = (!empty($title) ? $title . ' | ' :  $title) . '123job';

            }
        }
        else
        {
            $this->title = $title_page;
        }
    }

    /**
     * Meta description
     */
    private function setMetaDescription()
    {
        $total              = array_get($this->options,'total');
        $location           = array_get($this->options,'location');
        $keyword_search     = array_get($this->options,'keyword_search');
        $metaDescription    = array_get($this->options,'description_page', array_get($this->metaDataDefault, 'description'));

        $description = '';

        if ($total) $description = 'Xem ' . $total . ' việc làm ';
        if ($keyword_search)
        {
            $description .= $keyword_search ;
        }

        if (!$this->detail_page )
        {
            $description .= ','. $keyword_search . $this->suffixKeyword;
            if ($location) $description       .= $this->separatorLocation . $location ;
            $this->metaDescription = trim(trim(($description ? $description . ' , ' : ''  )  .$metaDescription,' | '),' ,');
        }
        else
        {
            $this->metaDescription = $metaDescription;
        }
    }


    /**
     * Set meta keyword tag
     */
    private function setMetakeyword()
    {
        $location           = array_get($this->options,'location');
        $keyword_search     = array_get($this->options,'keyword_search');
        $metaKeyword        = array_get($this->options,'keyword_page', array_get($this->metaDataDefault, 'keyword'));

        $keyword = (!$this->detail_page) ? $this->prefixKeyword : '';

        if ($keyword_search) $keyword .= $keyword_search;
        if ( !empty($metaKeyword)) $keyword .=  ', '.$keyword_search. $this->suffixKeyword;

        if ($location) $keyword       .= $this->separatorLocation . $location;

        $this->metaKeyword =  !empty($metaKeyword) ?  $keyword . ', ' . $metaKeyword : $keyword;
    }

    private function setImage()
    {
        $this->image = array_get($this->options,'image', array_get($this->metaDataDefault, 'image'));
    }

    public function renderMetaSeo()
    {
        $that  = $this;
        $meta  = '<title>'.clear_spaces_string_seo($that->title).'</title>';
        $meta .= '<meta name="description" content="'.clear_spaces_string_seo($that->metaDescription).'"/>';
        $meta .= '<meta name="keywords" itemprop="keywords" content="'.clear_spaces_string_seo($that->metaKeyword).'"/>';
        $meta .= '<meta name="language" content="vietnamese"/>';

        if ($that->image)
        {
            $image = strpos($that->image, '___') !== false ? pare_url_file($that->image) :  $that->image;
            if($image)
            {
                $meta .= '<meta itemprop="image" content="'.$image.'">
                            <meta property="og:image" content="'.$image.'" />
                            <meta property="og:image:width" content="'.$that->widthImage.'">
	                        <meta property="og:image:height" content="'. $that->heightImage .'">';
            }
        }

        $meta .= '<meta name="author" content="Việc làm 123job.vn">';
        $meta .= '<meta http-equiv="content-language" content="vi" />';
        $meta .= '<meta name="GENERATOR" content="123job.vn">';
        $meta .= '<meta name="copyright" content="Copyright © 2018 by 123job.vn">';
        $meta .= '<meta property="og:description" content="'.clear_spaces_string_seo($that->metaDescription).'">';
        $meta .= '<meta property="og:locale" content="vi_VN" />';
        $meta .= '<meta property="og:title" itemprop="name" content="'.clear_spaces_string_seo($that->title).'">';
        $meta .= '<meta property="og:site_name" content="123job | Việc làm mới hiệu quả, Tìm việc làm mới nhanh">';
        return $meta;
    }
}